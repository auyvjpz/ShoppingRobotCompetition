/*
 * arm.c
 *
 *  Created on: 2021年4月21日
 *      Author: Jpz
 */
#include "uart.h"
#include "arm.h"
#include "tasklist.h"
#include "flags.h"
#include "DSP2833x_Device.h"
#include "DSP2833x_Examples.h"
#include <string.h>
#include <stdlib.h>

#define ARM_TB_PRD 62500



struct ARM_Class arm;


/* tools */



/*********/


/**  机械臂动作 begin    **/
void arm_setDjUs(unsigned int d1,unsigned int d2,unsigned int d3,unsigned int d4,unsigned int d5,unsigned int d6)
{
    arm.d1 = d1;
    arm.d2 = d2;
    arm.d3 = d3;
    arm.d4 = d4;
    arm.d5 = d5;
    arm.d6 = d6;
    ARM_us2Cmps(&arm);
}

static char act_default(void)
{

    unsigned int lst[1][6]={  1500,    2120,   2140,   960,    1200,   1460};
    arm.actState = ARM_ACTION_DEFAULT;
    return TKL_addTask(1,&lst[0][0]);
}
static char act_watch_h(void)
{
    unsigned int lst[1][6]={  880,    1340,   1360,   1100,    1400,   1100};
    arm.actState = ARM_ACTION_WATCH_U;
    return TKL_addTask(1,&lst[0][0]);
}
static char act_watch_h_l(void)
{
    unsigned int lst[1][6]={  1000,    1340,   1380,   880,    1140,   1100};
    arm.actState = ARM_ACTION_WATCH_U;
    return TKL_addTask(1,&lst[0][0]);
}
static char act_watch_h_r(void)
{
    unsigned int lst[1][6]={  720,    1440,   1460,   1100,    1420,   1100};
    arm.actState = ARM_ACTION_WATCH_U;
    return TKL_addTask(1,&lst[0][0]);
}
static char act_watch_l(void)
{
    unsigned int lst[1][6]={  860,    860,   1000,   640,    980,   1100};
    arm.actState = ARM_ACTION_WATCH_D;
    return TKL_addTask(1,&lst[0][0]);
}
static char act_watch_l_l(void)
{
    unsigned int lst[1][6]={  980,    860,   1000,   620,    960,   1100};
    arm.actState = ARM_ACTION_WATCH_D;
    return TKL_addTask(1,&lst[0][0]);
}
static char act_watch_l_r(void)
{
    unsigned int lst[1][6]={  720,    860,   1000,   620,    960,   1100};
    arm.actState = ARM_ACTION_WATCH_D;
    return TKL_addTask(1,&lst[0][0]);
}

static char act_get_u_m(void)
{
    unsigned int lst[8][6]={  880,    1340,   1360,   1100,    1400,   1460,
                              880,    1340,   1180,   1120,    1240,   1460,
                              880,    1340,   1180,   1120,    1240,   1740,
                              880,    1340,   1420,   880,    1240,   1740,
                              1500,   1340,   1420,   880,    1240,   1740,
                              1660,     1400,   1140,   1080,    1140,   1740,
                              1660,     1400,   1140,   1080,    1140,   1460,
                              1500,     1380,   1440,   520,    900,   1460,};
    arm.actState = ARM_ACTION_GET_U_M;
    return TKL_addTask(8,&lst[0][0]);
}
static char act_get_u_l(void)
{
    unsigned int lst[8][6]={  1060,    1440,   1380,   880,    1140,   1460,
                              1060,     1440,   1140,   1420,    1440,   1460,
                              1060,     1440,   1140,   1420,    1440,   1740,
                              1060,     1440,   1460,   1060,    1440,   1740,
                              1500,     1440,   1460,   1060,    1440,   1740,
                              1660,     1400,   1140,   1080,    1140,   1740,
                              1660,     1400,   1140,   1080,    1140,   1460,
                              1500,     1380,   1440,   520,    900,   1460,};
    arm.actState = ARM_ACTION_GET_U_L;
    return TKL_addTask(8,&lst[0][0]);
}

static char act_get_u_r(void)
{
    unsigned int lst[8][6]={  700,    1440,   1460,   1100,    1420,   1460,
                              700,     1440,   1160,   1260,    1300,   1460,
                              700,     1440,   1160,   1260,    1300,   1740,
                              700,     1440,   1500,   940,    1300,   1740,
                              1500,     880,   1000,   500,    780,   1740,
                              1660,     1400,   1140,   1080,    1140,   1740,
                              1660,     1400,   1140,   1080,    1140,   1460,
                              1500,     1380,   1440,   520,    900,   1460,};
    arm.actState = ARM_ACTION_GET_U_R;
    return TKL_addTask(8,&lst[0][0]);
}

static char act_get_d_m(void)
{
    unsigned int lst[8][6]={  860,    820,   820,   620,    840,   1460,
                              860,    1000,   720,   620,    560,   1460,
                              860,    1000,   720,   620,    560,   1740,
                              860,    680,   780,   500,    780,   1740,
                              1500,     880,   1000,   500,    780,   1740,
                              1660,     1400,   1140,   1080,    1140,   1740,
                              1660,     1400,   1140,   1080,    1140,   1460,
                              1500,     1380,   1440,   520,    900,   1460,};
    arm.actState = ARM_ACTION_GET_D_M;
    return TKL_addTask(8,&lst[0][0]);
}
static char act_get_d_l(void)
{
    unsigned int lst[8][6]={  1080,    860,   820,   580,    780,   1460,
                              1080,    920,   600,   960,    780,   1460,
                              1080,    920,   600,   960,    780,   1740,
                              1080,    920,   920,   500,    820,   1740,
                              1500,     880,   1000,   500,    780,   1740,
                                1660,     1400,   1140,   1080,    1140,   1740,
                                1660,     1400,   1140,   1080,    1140,   1460,
                                1500,     1380,   1440,   520,    900,   1460,};
    arm.actState = ARM_ACTION_GET_D_L;
    return TKL_addTask(8,&lst[0][0]);
}

static char act_get_d_r(void)
{
    unsigned int lst[8][6]={  680,    920,   920,   500,    760,   1460,
                              680,    860,   600,   780,    660,   1460,
                              680,    860,   600,   780,    660,   1740,
                              1080,    920,   920,   500,    820,   1740,
                              1500,     880,   1000,   500,    780,   1740,
                              1660,     1400,   1140,   1080,    1140,   1740,
                              1660,     1400,   1140,   1080,    1140,   1460,
                              1500,     1380,   1440,   520,    900,   1460,};
    arm.actState = ARM_ACTION_GET_D_R;
    return TKL_addTask(8,&lst[0][0]);
}
static char act_stop(void)
{
    TKL_Init();
    arm.actState = ARM_ACTION_STOP;
    return 0;
}

static char act_watch_E(void)
{
    unsigned int lst[1][6]={  840  ,  2380,   1960,   680,    960,   900,
                              };
    arm.actState = ARM_ACTION_WATCH_E;
    return TKL_addTask(1,&lst[0][0]);
}

static char act_get_E(void)
{
    unsigned int lst[6][6]={  840,    2380,   1600,   1200,    1040,   1400,
                              840,    2420,   1580,   1300,    1080,   1760,
                              840,    2420,   1940,   880,    1180,   1760,
                              1600,    2420,   1940,   1040,    1160,   1760,
                              1600,    2420,   1940,   1040,    1160,   1480,
                              840,    2420,   1940,   880,    900,   1480,};
    arm.actState = ARM_ACTION_GET_E;
    return TKL_addTask(6,&lst[0][0]);
}




char action_Handler(char n)
{
    char ret;
    switch(n)
    {
        case ARM_ACTION_DEFAULT:      ret = act_default();      break;
        case ARM_ACTION_WATCH_U:      ret = act_watch_h();      break;
        case ARM_ACTION_WATCH_U_L:    ret = act_watch_h_l();    break;
        case ARM_ACTION_WATCH_U_R:    ret = act_watch_h_r();    break;
        case ARM_ACTION_WATCH_D:      ret = act_watch_l();      break;
        case ARM_ACTION_WATCH_D_L:    ret = act_watch_l_l();    break;
        case ARM_ACTION_WATCH_D_R:    ret = act_watch_l_r();    break;
        case ARM_ACTION_GET_U_L:      ret = act_get_u_l();      break;
        case ARM_ACTION_GET_U_M:      ret = act_get_u_m();      break;
        case ARM_ACTION_GET_U_R:      ret = act_get_u_r();      break;
        case ARM_ACTION_GET_D_L:      ret = act_get_d_l();      break;
        case ARM_ACTION_GET_D_M:      ret = act_get_d_m();      break;
        case ARM_ACTION_GET_D_R:      ret = act_get_d_r();      break;
        case ARM_ACTION_STOP:         ret = act_stop();         break;
        case ARM_ACTION_WATCH_E:      ret = act_watch_E();         break;
        case ARM_ACTION_GET_E:        ret = act_get_E();         break;
    }
    return ret;
}



/**  机械臂动作 end    **/

void ARM_Init(void)
{
    // 打开对应外设时钟
    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0; //先关闭时基模块时钟
    SysCtrlRegs.PCLKCR1.bit.EPWM3ENCLK = 1;
    SysCtrlRegs.PCLKCR1.bit.EPWM4ENCLK = 1;
    SysCtrlRegs.PCLKCR1.bit.EPWM5ENCLK = 1;
    EDIS;

    //epwm3
    InitEPwm3Gpio(); //引脚初始化

    //时基单元配置
    EPwm3Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE; //不同步
    EPwm3Regs.TBCTL.bit.PHSEN = TB_DISABLE; //不设置相位
    EPwm3Regs.TBPHS.half.TBPHS = 0;
    EPwm3Regs.TBCTR = 0; //清除计数
    EPwm3Regs.TBPRD = ARM_TB_PRD; //计数周期
    EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP; //向上计数
    EPwm3Regs.TBCTL.bit.HSPCLKDIV = 0x6;
    EPwm3Regs.TBCTL.bit.CLKDIV = 0x2;//50Hz
    //比较单元配置
    EPwm3Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm3Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW; //影子模式
    EPwm3Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
    EPwm3Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;//零时刻装载
    EPwm3Regs.CMPA.half.CMPA = 0;
    EPwm3Regs.CMPB = 0; //设置初始比较值为零
    //动作单元配置
    EPwm3Regs.AQCTLA.bit.ZRO = AQ_SET; //计数为零时 高电平
    EPwm3Regs.AQCTLA.bit.CAU = AQ_CLEAR; //计数为向上A比较点时 低电平
    EPwm3Regs.AQCTLB.bit.ZRO = AQ_SET; //计数为零时 高电平
    EPwm3Regs.AQCTLB.bit.CBU = AQ_CLEAR; //计数为向上B比较点时 低电平
    //事件触发单元配置   中断配置
    EPwm3Regs.ETSEL.bit.INTSEL = ET_CTR_PRD;//计数为0时进入中断
    EPwm3Regs.ETSEL.bit.INTEN = 0; //开启中断
    EPwm3Regs.ETPS.bit.INTPRD = ET_1ST;
    EPwm3Regs.ETCLR.bit.INT = 1;//清除中断标志
    //epwm4
    InitEPwm4Gpio(); //引脚初始化

    //时基单元配置
    EPwm4Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE; //不同步
    EPwm4Regs.TBCTL.bit.PHSEN = TB_DISABLE; //不设置相位
    EPwm4Regs.TBPHS.half.TBPHS = 0;
    EPwm4Regs.TBCTR = 0; //清除计数
    EPwm4Regs.TBPRD = ARM_TB_PRD; //计数周期
    EPwm4Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP; //向上计数
    EPwm4Regs.TBCTL.bit.HSPCLKDIV = 0x6;
    EPwm4Regs.TBCTL.bit.CLKDIV = 0x2;//TBCLK == SYSCLK == 150MHz
    //比较单元配置
    EPwm4Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm4Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW; //影子模式
    EPwm4Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
    EPwm4Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;//零时刻装载
    EPwm4Regs.CMPA.half.CMPA = 0;
    EPwm4Regs.CMPB = 0; //设置初始比较值为零
    //动作单元配置
    EPwm4Regs.AQCTLA.bit.ZRO = AQ_SET; //计数为零时 高电平
    EPwm4Regs.AQCTLA.bit.CAU = AQ_CLEAR; //计数为向上A比较点时 低电平
    EPwm4Regs.AQCTLB.bit.ZRO = AQ_SET; //计数为零时 高电平
    EPwm4Regs.AQCTLB.bit.CBU = AQ_CLEAR; //计数为向上B比较点时 低电平
    //事件触发单元配置   中断配置
    EPwm4Regs.ETSEL.bit.INTSEL = ET_CTR_PRD;//计数为0时进入中断
    EPwm4Regs.ETSEL.bit.INTEN = 0; //不开启中断
    EPwm4Regs.ETPS.bit.INTPRD = ET_1ST;

    //epwm5
    InitEPwm5Gpio(); //引脚初始化

    //时基单元配置
    EPwm5Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE; //不同步
    EPwm5Regs.TBCTL.bit.PHSEN = TB_DISABLE; //不设置相位
    EPwm5Regs.TBPHS.half.TBPHS = 0;
    EPwm5Regs.TBCTR = 0; //清除计数
    EPwm5Regs.TBPRD = ARM_TB_PRD; //计数周期
    EPwm5Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP; //向上计数
    EPwm5Regs.TBCTL.bit.HSPCLKDIV = 0x6;
    EPwm5Regs.TBCTL.bit.CLKDIV = 0x2;//TBCLK == SYSCLK == 150MHz
    //比较单元配置
    EPwm5Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm5Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW; //影子模式
    EPwm5Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
    EPwm5Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;//零时刻装载
    EPwm5Regs.CMPA.half.CMPA = 0;
    EPwm5Regs.CMPB = 0; //设置初始比较值为零
    //动作单元配置
    EPwm5Regs.AQCTLA.bit.ZRO = AQ_SET; //计数为零时 高电平
    EPwm5Regs.AQCTLA.bit.CAU = AQ_CLEAR; //计数为向上A比较点时 低电平
    EPwm5Regs.AQCTLB.bit.ZRO = AQ_SET; //计数为零时 高电平
    EPwm5Regs.AQCTLB.bit.CBU = AQ_CLEAR; //计数为向上B比较点时 低电平
    //事件触发单元配置   中断配置
    EPwm5Regs.ETSEL.bit.INTSEL = ET_CTR_PRD;//计数为0时进入中断
    EPwm5Regs.ETSEL.bit.INTEN = 0; //不开启中断
    EPwm5Regs.ETPS.bit.INTPRD = ET_1ST;

    //属性
    arm.d1 = 1500;
    arm.cmp1 = us2Cmp(arm.d1);
    D1CMP = arm.cmp1;

    arm.d2 = 1500;
    arm.cmp2 = us2Cmp(arm.d2);
    D2CMP = arm.cmp2;

    arm.d3 = 1500;
    arm.cmp3 = us2Cmp(arm.d3);
    D3CMP = arm.cmp3;

    arm.d4 = 1500;
    arm.cmp4 = us2Cmp(arm.d4);
    D4CMP = arm.cmp4;

    arm.d5 = 1500;
    arm.cmp5 = us2Cmp(arm.d5);
    D5CMP = arm.cmp5;

    arm.d6 = 1500;
    arm.cmp6 = us2Cmp(arm.d6);
    D6CMP = arm.cmp6;

    arm.actState = ARM_ACTION_STOP;
    arm.action = action_Handler;
    TKL_Init();

    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1; //打开时基模块时钟
    EDIS;
}

unsigned int us2Cmp(unsigned int us)
{
//    float tmp;
    if(us < 500)
        us = 500;
    else if(us > 2500)
        us = 2500;
    return (long)us*ARM_TB_PRD/20000;
}

int absStepNum(int a)
{
    if(a<0)
        a = -a;
    if(a%CMPSTEP == 0)
        return a/CMPSTEP;
    else
        return a/CMPSTEP + 1;
}
unsigned int cntStepNum(unsigned int *l)
{
    unsigned int ret,i;
    for(i=0;i<6;i++)
    {
        if(l[i] == 0) l[i] = 1;
    }
    ret = l[0];
    for(i=1;i<6;i++)
    {
        if(ret<l[i]) ret = l[i];
    }
    return ret;
}

unsigned int delt[7] = {0,0,0,0,0,0,0};

char ARM_Active(struct ARM_Class *arm)
{
    static char flag=0;
    static float k_;

    if(arm->actState != ARM_ACTION_STOP && flag == 0){//计算总步长
        TKL_setTask();
        ARM_us2Cmps(arm);

        delt[1] = absStepNum(arm->cmp1 - D1CMP);
        delt[2] = absStepNum(arm->cmp2 - D2CMP);
        delt[3] = absStepNum(arm->cmp3 - D3CMP);
        delt[4] = absStepNum(arm->cmp4 - D4CMP);
        delt[5] = absStepNum(arm->cmp5 - D5CMP);
        delt[6] = absStepNum(arm->cmp6 - D6CMP);

        delt[0] = cntStepNum(&delt[1]);//取出最大步数
        k_ = delt[0];

        flag=1;
    }
    if(flag == 1 && delt[0] != 0)
    {

        D1CMP = setCmp(arm->cmp1, D1CMP,(float)delt[1]/k_);
        D2CMP = setCmp(arm->cmp2, D2CMP,(float)delt[2]/k_);
        D3CMP = setCmp(arm->cmp3, D3CMP,(float)delt[3]/k_);
        D4CMP = setCmp(arm->cmp4, D4CMP,(float)delt[4]/k_);
        D5CMP = setCmp(arm->cmp5, D5CMP,(float)delt[5]/k_);
        D6CMP = setCmp(arm->cmp6, D6CMP,(float)delt[6]/k_);

        delt[0] -= 1;
        if( delt[0] == 0 )
        {
            D1CMP = arm->cmp1;
            D2CMP = arm->cmp2;
            D3CMP = arm->cmp3;
            D4CMP = arm->cmp4;
            D5CMP = arm->cmp5;
            D6CMP = arm->cmp6;

            TKL_nextTask();//
            flag = 0;
            if(*nowTask==0)
            {
                arm->actState = ARM_ACTION_STOP;
            }
        }
    }
    arm->taskNum = *nowTask;
    return arm->actState;
}

unsigned int setCmp(unsigned int exp, unsigned int now,float k)
{
    unsigned int delt;

    if(exp > now)
    {
        delt = exp - now;
        if(delt > CMPSTEP)
            return now + k*CMPSTEP;
        else
            return now + k*delt;
    }
    else
    {
        delt = now - exp;
        if(delt > CMPSTEP)
            return now - k*CMPSTEP;
        else
            return now - k*delt;
    }
}

void ARM_us2Cmps(struct ARM_Class *arm)
{
    if(arm->d1 > 2500) arm->d1 = 2500;
    else if(arm->d1 < 500) arm->d1 = 500;
    arm->cmp1 = us2Cmp(arm->d1);

    if(arm->d2 > 2500) arm->d2 = 2500;
    else if(arm->d2 < 500) arm->d2 = 500;
    arm->cmp2 = us2Cmp(arm->d2);

    if(arm->d3 > 2500) arm->d3 = 2500;
    else if(arm->d3 < 500) arm->d3 = 500;
    arm->cmp3 = us2Cmp(arm->d3);

    if(arm->d4 > 2500) arm->d4 = 2500;
    else if(arm->d4 < 500) arm->d4 = 500;
    arm->cmp4 = us2Cmp(arm->d4);

    if(arm->d5 > 2500) arm->d5 = 2500;
    else if(arm->d5 < 500) arm->d5 = 500;
    arm->cmp5 = us2Cmp(arm->d5);

    if(arm->d6 > 2500) arm->d6 = 2500;
    else if(arm->d6 < 500) arm->d6 = 500;
    arm->cmp6 = us2Cmp(arm->d6);
}

unsigned int str2Us(char *p)
{
    unsigned int us = 0;
    while(strchr("0123456789",*p)!=NULL)
    {
        us = us*10 + ( *p - '0');
        p++;
        if(us>2500)
        {us = 2500; break;}
    }
    return us;
}


void ARM_SendDataToCtrlDev(void)
{
    char tmp[]={'\0','\0','\0'};
    SCI1_sendString("arm");
    SCI1_sendString("D1");
    tmp[0]=(arm.d1 & 0xff00)>>8;
    tmp[1]=(arm.d1 & 0x00ff);
    SCI1_sendString(tmp);

    SCI1_sendString("D2");
    tmp[0]=(arm.d2 & 0xff00)>>8;
    tmp[1]=(arm.d2 & 0x00ff);
    SCI1_sendString(tmp);

    SCI1_sendString("D3");
    tmp[0]=(arm.d3 & 0xff00)>>8;
    tmp[1]=(arm.d3 & 0x00ff);
    SCI1_sendString(tmp);

    SCI1_sendString("D4");
    tmp[0]=(arm.d4 & 0xff00)>>8;
    tmp[1]=(arm.d4 & 0x00ff);
    SCI1_sendString(tmp);

    SCI1_sendString("D5");
    tmp[0]=(arm.d5 & 0xff00)>>8;
    tmp[1]=(arm.d5 & 0x00ff);
    SCI1_sendString(tmp);

    SCI1_sendString("D6");
    tmp[0]=(arm.d6 & 0xff00)>>8;
    tmp[1]=(arm.d6 & 0x00ff);
    SCI1_sendString(tmp);
    SCI1_sendString("end\r\n");

}
