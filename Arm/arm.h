/*
 * arm.h
 *
 *  Created on: 2021年4月21日
 *      Author: Jpz
 */

#ifndef _ARM_H_
#define _ARM_H_

#define D1CMP EPwm3Regs.CMPA.half.CMPA
#define D2CMP EPwm4Regs.CMPA.half.CMPA
#define D3CMP EPwm3Regs.CMPB
#define D4CMP EPwm5Regs.CMPA.half.CMPA
#define D5CMP EPwm4Regs.CMPB
#define D6CMP EPwm5Regs.CMPB

#define CMPSTEP 10

#define ARM_ACTION_STOP       0
#define ARM_ACTION_DEFAULT    1
#define ARM_ACTION_GET_U_L    4
#define ARM_ACTION_GET_U_M    5
#define ARM_ACTION_GET_U_R    6
#define ARM_ACTION_GET_D_L    7
#define ARM_ACTION_GET_D_M    8
#define ARM_ACTION_GET_D_R    9
#define ARM_ACTION_WATCH_U    2
#define ARM_ACTION_WATCH_D    3
#define ARM_ACTION_WATCH_U_L    10
#define ARM_ACTION_WATCH_U_R    11
#define ARM_ACTION_WATCH_D_L    12
#define ARM_ACTION_WATCH_D_R    13

#define ARM_ACTION_WATCH_E      14
#define ARM_ACTION_GET_E        15


struct ARM_Class
{
    unsigned int d1;//04  3A    标记高电平时间
    unsigned int cmp1;//04  3A  标记需要的比较值
    unsigned int d2;//06  4A
    unsigned int cmp2;//06  4A
    unsigned int d3;//05  3B
    unsigned int cmp3;//05  3B
    unsigned int d4;//08  5A
    unsigned int cmp4;//08  5A
    unsigned int d5;//07  4B
    unsigned int cmp5;//07  4B
    unsigned int d6;//09  5B
    unsigned int cmp6;//09  5B

    unsigned int taskNum;
    char actState;
    char (* action)(char);
};

extern struct ARM_Class arm;

void ARM_Init(void);
char ARM_Active(struct ARM_Class *arm);
void ARM_us2Cmps(struct ARM_Class *arm);


unsigned int us2Cmp(unsigned int us);
unsigned int str2Us(char *p);
unsigned int setCmp(unsigned int exp, unsigned int now,float k);

void ARM_SendDataToCtrlDev(void);

//interrupt void ARM_ISQ(void);

#endif /* _ARM_H_ */
