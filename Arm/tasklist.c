/*
 * tasklist.c
 *
 *  Created on: 2021年5月12日
 *      Author: Jpz
 */
#include "tasklist.h"
#include "arm.h"

unsigned int taskList[TASK_MAXLEN][TASK_TJC+1];
unsigned int *nowTask;


void TKL_Init(void)
{
    unsigned int j;
    for(j=0;j<TASK_MAXLEN;j++)
    {
        taskList[j][0] = 0;
    }
    nowTask = &taskList[0][0];
}

char TKL_addTask(char n,unsigned int *lst)
{
    char i;
    unsigned int *p;
    unsigned int *l;

    if(n>TASK_MAXLEN)
        return -1;//动作过多报错

    if(*nowTask != 0)
        return -2;//当前正忙

    p = nowTask;
    l = lst;
    for(;n>0;n--)
    {
        *p = n;
        for(i=0;i<TASK_TJC;i++)
        {
            *(p+i+1) = *(l+i);
        }
        l += TASK_TJC;
        p += TASK_TJC+1;
        if(p > &taskList[TASK_MAXLEN-1][0])
            p = &taskList[0][0];
    }

    return 0;
}

char TKL_setTask(void)
{
    if(*nowTask == 0)
        return 0;//没有等待的任务 不需要赋值

    arm.d1 = *(nowTask+1);
    arm.d2 = *(nowTask+2);
    arm.d3 = *(nowTask+3);
    arm.d4 = *(nowTask+4);
    arm.d5 = *(nowTask+5);
    arm.d6 = *(nowTask+6);
    return *nowTask;
}

void TKL_nextTask(void)
{
    *nowTask = 0;//标记当前已完成
    nowTask += (TASK_TJC+1); //切换到下一个
    if(nowTask > &taskList[TASK_MAXLEN-1][0])
        nowTask = &taskList[0][0];
}




