/*
 * tasklist.h
 *
 *  Created on: 2021年5月12日
 *      Author: Jpz
 */

#ifndef _TASKLIST_H_
#define _TASKLIST_H_


#define TASK_MAXLEN     10  //最大任务数
#define TASK_TJC        6   //Number of joints 关节数

extern unsigned int taskList[][];
extern unsigned int *nowTask;

void TKL_Init(void);
char TKL_addTask(char n,unsigned int *lst);
char TKL_setTask(void);
void TKL_nextTask(void);


#endif /* _TASKLIST_H_ */
