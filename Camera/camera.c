/*
 * camera.c
 *
 *  Created on: 2021年5月14日
 *      Author: Jpz
 */
#include "DSP2833x_Device.h"
#include "DSP2833x_Examples.h"
#include "camera.h"
#include "uart.h"
#include "flags.h"
#include "huskylens.h"
#include <string.h>

struct CMR_Class camera;

//#define UNO
#define NON

//static char checkRet(char *tmp,char n)
//{
//    char i,ret;
//    char symbol[20];
//    memset(symbol, 0, 20);
//    for(i=0;i<n;i++)
//    {
//        symbol[tmp[i]] += 1;
//    }
//    ret = 0;
//    for(i=1;i<20;i++)
//    {
//        if(symbol[i] > symbol[ret])
//            ret = i;
//    }
//    return ret;
//}

char cmr_classify(char n)
{
    char ret;
    if(GpioDataRegs.GPBDAT.bit.GPIO50 == 0)
        return CMR_NOTDOIT;

    ret = COMMAND_REQUEST_BLOCKS();
    if(ret == 0x01||ret == 0x02||ret == 0x03||ret == 0x04||ret == 0x05||ret == 0x06||ret == 0x07||ret == 0x08)
        return CMR_DOIT;
    else
        return CMR_NOTDOIT;


//    char i=0,I=0,ret;
//    static char tmp[8] = {0,0,0,0,0,0,0,0};
//    if(GpioDataRegs.GPBDAT.bit.GPIO50 == 0)
//        return CMR_NOTDOIT;
//    camera.state = 1;
//    while(I < n)
//    {
//
//        memset(tmp, '\0', 8);
//        ret = COMMAND_REQUEST_BLOCKS();
//        for(i=0;i<n;i++)
//        {
//            tmp[i] = COMMAND_REQUEST_BLOCKS();
//            if(tmp[i] != ret)
//                return CMR_NOTDOIT;
//        }
//        if(i == n)
//        {
//            camera.state = 0;
//            if(ret == 0x01||ret == 0x02||ret == 0x03||ret == 0x04||ret == 0x05||ret == 0x06||ret == 0x07||ret == 0x08)
//                return CMR_DOIT;
//            else
//                return CMR_NOTDOIT;
//        }
//        I++;
//    }
//    camera.state = 0;
//    return CMR_NOTDOIT;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//#ifdef UNO
//    unsigned long i = 0;
//    char ret[8] = {0,0,0,0,0,0,0,0};
//    char p=0;
//    char f=0;
//
//    if(GpioDataRegs.GPBDAT.bit.GPIO50 == 0)
//        return CMR_NOTDOIT;
//    SCI1_sendString("shi bie kai shi\r\n");
//    camera.state = 1;
////    while(flag.works == 0);flag.works = 0;
//
//    while(f==0)
//    {
//        do
//        {
//            i++;
//            if(i % 1000000 == 0)
//            {
//                while(ScibRegs.SCICTL2.bit.TXEMPTY == 0);
//                ScibRegs.SCITXBUF = 0x00;
//            }
//            if(i > 10000000)//防止死机
//            {
//                SCI1_sendString("wu fan hui\r\n");
//                return CMR_NOTDOIT;
//            }
//        }while(ScibRegs.SCIRXST.bit.RXRDY != 1);
//        ret[p] = ScibRegs.SCIRXBUF.bit.RXDT;
//        p++;
//
//        if(&ret[p] - &ret[0] >= n)
//        {
//            f=1;//假设结果相同
//            for(i=0;i<n;i++)
//            {
//                if(ret[0]!=ret[i])
//                { f = 0; break; }
//            }
//            p=0;
//        }
//    }
//    camera.state = 0;
//    if(ret[0] == 0x31)
//    {
//        SCI1_sendString(&ret[1]);
//        return CMR_DOIT;
//    }
//    else
//    {
//        SCI1_sendString(&ret[1]);
//        return CMR_NOTDOIT;
//    }
//#endif


}


void CMR_Init(void)
{
    SCI2_Init(9600);
    camera.state = 0;
    camera.classify = cmr_classify;
}
