/*
 * camera.h
 *
 *  Created on: 2021��5��14��
 *      Author: Jpz
 */

#ifndef _CAMERA_H_
#define _CAMERA_H_

#define CMR_NULL  0
#define CMR_HZ    1
#define CMR_LZ    2
#define CMR_MF    3
#define CMR_WQ    4
#define CMR_XH    5
#define CMR_HN    6
#define CMR_WHH   7
#define CMR_TLS   8

#define CMR_DOIT        1
#define CMR_NOTDOIT     0

struct CMR_Class{
    char state;
    char ret;
    char (*classify)(char);
};
extern struct CMR_Class camera;

void CMR_Init(void);


#endif /* _CAMERA_H_ */
