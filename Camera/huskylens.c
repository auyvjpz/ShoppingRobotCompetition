/*
 * huskylens.c
 *
 *  Created on: 2021��5��27��
 *      Author: Administrator
 */

#include "huskylens.h"
#include "uart.h"
#include <string.h>

unsigned char COMMAND_REQUEST_BLOCKS(void)
{
    unsigned char *p;
    unsigned char buf[35];
    int i=0;
    unsigned char dat[] = {0x55, 0xAA, 0x11, 0x00, 0x21, 0x31};
    memset((void *)buf, '\0', 35);
    while(i<2)
    {
        SCI2_sendDatas((char *)dat,6);
        SCI2_reciveDatas((char *)buf,32,99999);
        p = buf;
        while(p<buf+35)
        {
            p++;
            if(p[0] == 0x55 && p[1] == 0xaa && p[2] == 0x11 && p[3] == 0x0a && p[4] == 0x2a )
                return (p[13]+p[14]*10);
        }
        i++;
    }
    return 0;
}

