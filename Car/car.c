/*
 * car.c
 *
 *  Created on: 2021年4月15日
 *      Author: jpz
 */

//#include "DSP2833x_Device.h"
//#include "DSP2833x_Examples.h"
#include "car.h"
#include "moto.h"
#include "ctrl.h"
#include "io.h"
#include "uart.h"


struct CAR_class car;
/*  tools  */

//static char stepToRim(int m)
//{}

/* 对象动作函数 begin */

char CAR_MoveToX(int x_Exp)//结束后车头向  B  D
{
    if(car.axis_x == x_Exp)
    {
        car.cmd = Stop;
        return 0;
    }
    if(car.axis_dir==faceToA)
    {
        if(car.axis_x < x_Exp)        car.cmd = TurnLeft;
        else if(car.axis_x > x_Exp)   car.cmd = TurnRight;
    }
    else if(car.axis_dir==faceToC)
    {
        if(car.axis_x < x_Exp)        car.cmd = TurnRight;
        else if(car.axis_x > x_Exp)   car.cmd = TurnLeft;
    }
    else if(car.axis_dir==faceToB)
    {
        if(car.axis_x < x_Exp)        car.cmd =  Forward;
        else if(car.axis_x > x_Exp)   car.cmd =  Backward;
    }
    else if(car.axis_dir==faceToD)
    {
        if(car.axis_x < x_Exp)        car.cmd =  Backward;
        else if(car.axis_x > x_Exp)   car.cmd =  Forward;
    }
    return 1;
}

char CAR_MoveToY(int y_Exp)//结束后车头向  A  C
{
    if(car.axis_y == y_Exp)
    {
        car.cmd = Stop;
        return 0;
    }
    if(car.axis_dir==faceToA)
    {
        if(car.axis_y < y_Exp)        car.cmd = Backward;
        else if(car.axis_y > y_Exp)   car.cmd = Forward;
    }
    else if(car.axis_dir==faceToC)
    {
        if(car.axis_y < y_Exp)        car.cmd = Forward;
        else if(car.axis_y > y_Exp)   car.cmd = Backward;
    }
    else if(car.axis_dir==faceToB)
    {
        if(car.axis_y < y_Exp)        car.cmd =  TurnLeft;
        else if(car.axis_y > y_Exp)   car.cmd =  TurnRight;
    }
    else if(car.axis_dir==faceToD)
    {
        if(car.axis_y < y_Exp)        car.cmd =  TurnRight;
        else if(car.axis_y > y_Exp)   car.cmd =  TurnLeft;
    }
    return 1;
}

static char CAR_MoveToExpXY(void)
{//flag:阶段标志 0代表已完成
    static char flag=0,x_i=0,y_i=0,d_i=0;
    if(car.exp_x != x_i || car.exp_y != y_i || car.exp_dir != d_i)
    {
        x_i = car.exp_x;
        y_i = car.exp_y;
        d_i = car.exp_dir;
        flag = 1;
    }
    if(car.exp_x == car.axis_x && car.exp_y == car.axis_y && car.exp_dir == car.axis_dir)
        return 0;

    if(car.exp_dir == faceToA || car.exp_dir == faceToC)//先走x 再走y
    {
        if(flag == 1)
        {
            if(CAR_MoveToX(x_i) == 0) flag=2;
        }
        else if(flag == 2)
        {
            if(car.axis_dir == faceToB && car.exp_dir == faceToA)       car.cmd = TurnRight;
            else if(car.axis_dir == faceToB && car.exp_dir == faceToC)  car.cmd = TurnLeft;
            else if(car.axis_dir == faceToD && car.exp_dir == faceToA)  car.cmd = TurnLeft;
            else if(car.axis_dir == faceToD && car.exp_dir == faceToC)  car.cmd = TurnRight;

            if(car.axis_dir == car.exp_dir) flag=3;
        }
        else if(flag == 3)
        {
            if(CAR_MoveToY(y_i) == 0) flag=0;
        }
    }
    else if(car.exp_dir == faceToD || car.exp_dir == faceToB)//先走y 再走x
    {
        if(flag == 1)
        {
            if(CAR_MoveToY(y_i) == 0) flag=2;
        }
        else if(flag == 2)
        {
            if(car.axis_dir == faceToA && car.exp_dir == faceToB)       car.cmd = TurnLeft;
            else if(car.axis_dir == faceToA && car.exp_dir == faceToD)  car.cmd = TurnRight;
            else if(car.axis_dir == faceToC && car.exp_dir == faceToB)  car.cmd = TurnRight;
            else if(car.axis_dir == faceToC && car.exp_dir == faceToD)  car.cmd = TurnLeft;

            if(car.axis_dir == car.exp_dir) flag=3;
        }
        else if(flag == 3)
        {
            if(CAR_MoveToX(x_i) == 0) flag=0;
        }
    }
    return flag;
}



/* 对象动作函数 end */

void CAR_Init(void)
{
    car.axis_dir = faceToB;
    car.axis_x = 0;
    car.axis_y = 3;

    car.exp_dir = car.axis_dir;
    car.exp_x = 0;
    car.exp_y = 3;

    car.cmd = Stop;
    car.self = car.cmd;
    car.pack=0;

    car.moveToXYD = CAR_MoveToExpXY;
}

void CAR_Stop(void)
{
    moto_l.duty = 0;
    moto_r.duty = 0;
    MOTO_updateMotoState(moto_l);
    MOTO_updateMotoState(moto_r);
}

void CAR_Forward(void)
{
    moto_l.dir = ToForward;
    moto_r.dir = ToForward;
    moto_l.duty = ctrlDuty.forward_duty_l;
    moto_r.duty = ctrlDuty.forward_duty_r;
    MOTO_updateMotoState(moto_l);
    MOTO_updateMotoState(moto_r);
}
void CAR_Backward(void)
{
    moto_l.dir = ToBackward;
    moto_r.dir = ToBackward;
    moto_l.duty = ctrlDuty.backward_duty_l;
    moto_r.duty = ctrlDuty.backward_duty_r;
    MOTO_updateMotoState(moto_l);
    MOTO_updateMotoState(moto_r);
}
void CAR_TurnLeft(void)
{
    static char flag=0;

    if(flag == 0)//转向
    {
        moto_l.dir = ToBackward;
        moto_r.dir = ToForward;
        moto_l.duty = ctrlDuty.turnleft_duty_l;
        moto_r.duty = ctrlDuty.turnleft_duty_r;
        MOTO_updateMotoState(moto_l);
        MOTO_updateMotoState(moto_r);
        if(io_head.io2 == L && io_head.io3 == L && io_head.io4 == L)
        {
            flag = 1;
        }
    }
    else if(flag ==1)//车头找到线
    {
        if(io_head.io2 == H||io_head.io3 == H)
        {
            CAR_Stop();
            flag=2;
        }
    }
    else if(flag ==2)//向前摆正姿态
    {
        CTRL_CtrlDev(&ctrlDuty, io_head.value,Forward);
        CAR_Forward();
        if(IO_checkRisingEdge(io_head))
        {
            CAR_Stop();
            flag=0;
            car.axis_dir++;
            if((int)car.axis_dir>3)
                car.axis_dir=faceToA;
            car.cmd = Forward;
        }
    }
}
void CAR_TurnRight(void)
{
    static char flag=0;
    if(flag == 0)//转向
    {
        moto_l.dir = ToForward;
        moto_r.dir = ToBackward;
        moto_l.duty = ctrlDuty.turnright_duty_l;
        moto_r.duty = ctrlDuty.turnright_duty_r;
        MOTO_updateMotoState(moto_l);
        MOTO_updateMotoState(moto_r);
        if(io_head.io2 == L && io_head.io1 == L && io_head.io0 == L)
        {
            flag = 1;
        }
    }
    if(flag ==1)
    {
        if(io_head.io2 == H||io_head.io1 == H)
        {
            CAR_Stop();
            flag=2;
        }
    }
    if(flag ==2)//向前摆正姿态
    {
        CTRL_CtrlDev(&ctrlDuty, io_head.value,Forward);
        CAR_Forward();
        if(IO_checkRisingEdge(io_head))
        {
            CAR_Stop();
            flag=0;
            car.axis_dir--;
            if(car.axis_dir<0)
                car.axis_dir=faceToD;
            car.cmd = Forward;
        }
    }

}

void CAR_MoveByCmd(void)
{
    if(car.cmd != car.self)
    {
        CAR_Stop();
        car.self = car.cmd;
    }
    switch(car.self)
    {
        case Stop:
            CAR_Stop();
            break;

        case Forward:
            CAR_Forward();
            break;

        case Backward:
            CAR_Backward();
            break;

        case TurnLeft:
            CAR_TurnLeft();
            break;

        case TurnRight:
            CAR_TurnRight();
            break;
//        case TurnBack:
//            CAR_TurnBack();
//            break;
    }
    //计算坐标
    CAR_RecordXY();
}


void CAR_RecordXY(void)//记录实际坐标
{
    if(car.self == Stop || car.self == TurnLeft || car.self == TurnRight)
        return ;//上述情况不需要改坐标
    if( IO_checkRisingEdge(io_tail) == 0)
        return ;//没有触发上升沿
    if(car.self == Forward)
    {
        switch(car.axis_dir)
        {
            case faceToA:
                car.axis_y--;
                break;
            case faceToB:
                car.axis_x++;
                break;
            case faceToC:
                car.axis_y++;
                break;
            case faceToD:
                car.axis_x--;
                break;
        }
    }
    else if(car.self == Backward)
    {
        switch(car.axis_dir)
        {
            case faceToA:
                car.axis_y++;
                break;
            case faceToB:
                car.axis_x--;
                break;
            case faceToC:
                car.axis_y--;
                break;
            case faceToD:
                car.axis_x++;
                break;
        }
    }

    //完成一次坐标更新让车停下
    CAR_Stop();
    car.cmd = Stop;
//    SCI1_sendString("car.axis_x:");
//    SCI1_sendString(float2str((float)car.axis_x,1));
//    SCI1_sendString("、car.axis_y:");
//    SCI1_sendString(float2str((float)car.axis_y,1));
//    SCI1_sendString("\r\n");
}

//int absDelt(int a, int b)
//{
//    if(a>b)
//        return a-b;
//    if(a<b)
//        return b-a;
//    if(a == b)
//        return 0;
//}


void CAR_SendDataToCtrlDev(void)
{
    SCI1_sendString("cnt");
    SCI1_sendString("x:");
    SCI1_sendString(uint2str(car.axis_x));
    SCI1_sendString("y:");
    SCI1_sendString(uint2str(car.axis_y));
    SCI1_sendString("d:");
    SCI1_sendString(uint2str((int)car.axis_dir));
    SCI1_sendString("p:");
    SCI1_sendString(uint2str((int)car.pack));
    SCI1_sendString("s:");
    SCI1_sendString(uint2str((int)car.self));
    SCI1_sendString("\r\n");
}




