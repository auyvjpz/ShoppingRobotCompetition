/*
 * car.h
 *
 *  Created on: 2021年4月15日
 *      Author: jpz
 */

#ifndef _CAR_H_
#define _CAR_H_

#define faceToA 0
#define faceToB 1
#define faceToC 2
#define faceToD 3
//enum dirState{
//    faceToA,
//    faceToB,
//    faceToC,
//    faceToD
//};
enum moveState{
    Stop,
    Forward,
    Backward,
    TurnLeft,
    TurnRight,
    TurnBack
};


struct CAR_class
{
    int axis_x;
    int axis_y;
    int axis_dir;
    enum moveState self;

    int exp_x;
    int exp_y;
    int exp_dir;
    enum moveState cmd;

    char (* moveToXYD)();

    int pack;
};

extern struct CAR_class car;


void CAR_Init(void);

void CAR_Stop(void);
void CAR_Forward(void);
void CAR_Backward(void);
void CAR_TurnLeft(void);
void CAR_TurnRight(void);
void CAR_MoveByCmd(void);

void CAR_RecordXY(void);//记录实际坐标

//char CAR_MoveToExpXY(void);

void CAR_SendDataToCtrlDev(void);
#endif /* _CAR_H_ */
