/*
 * ctrl.c
 *
 *  Created on: 2021��4��16��
 *      Author: Jpz
 */

#include "ctrl.h"
#include "io.h"
#include "moto.h"
#include "car.h"
#include "flags.h"

float KP = 8.42;
float KI = 0.02;
float KD = 1.23;


#define DUTY_MAX 60
#define STEP 0.05
#define DUTY_SLOW 12.5
#define DUTY_DEFAULT 17.5
#define DUTY_TURN_F 13.5
#define DUTY_TURN_B 16.5

float DUTY=DUTY_DEFAULT;



struct CTRL_carDuty ctrlDuty = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

void CTRL_Init(void)
{
    ctrlDuty.forward_duty_l = DUTY_DEFAULT;
    ctrlDuty.forward_duty_r = DUTY_DEFAULT;

    ctrlDuty.backward_duty_l = DUTY_DEFAULT;
    ctrlDuty.backward_duty_r = DUTY_DEFAULT;

    ctrlDuty.turnleft_duty_l = DUTY_TURN_B;
    ctrlDuty.turnleft_duty_r = DUTY_TURN_F;

    ctrlDuty.turnright_duty_l = DUTY_TURN_F;
    ctrlDuty.turnright_duty_r = DUTY_TURN_B;

}

float dutyLimit(float duty)
{
    if(duty < 0.0)
        return 0.0;
    if(duty > DUTY_MAX)
        return DUTY_MAX;
    return duty;
}

int abs(int a)
{
    if(a<0) return -a;
    else return a;
}

void CTRL_CtrlDev(struct CTRL_carDuty *ctrlDuty, int eor, enum moveState mode)
{
    int delt;
    delt = abs(car.axis_x - car.exp_x) + abs(car.axis_y - car.exp_y);
    if(flag.speed == 0 || flag.speed == 1)
    {
        eor -= PZ;
        if(delt == 1 || mode == Backward)
            DUTY = DUTY_SLOW;
        else
            DUTY = DUTY_DEFAULT;
    }
    else if(flag.speed == 2)
    {
        DUTY = 10.0;
        eor -= NS;
    }
    else if(flag.speed == 3)
    {
        DUTY = 10.0;
//        eor -= ZO;
    }

    switch(mode)
    {
        case Forward:
            ctrlDuty->forward_u = KP*eor + KI*(eor+ctrlDuty->forward_eor_1+ctrlDuty->forward_eor_2) + KD*(eor-ctrlDuty->forward_eor_1);
            ctrlDuty->forward_eor_2 = ctrlDuty->forward_eor_1;
            ctrlDuty->forward_eor_1 = eor;
            ctrlDuty->forward_duty_l = dutyLimit( DUTY - ctrlDuty->forward_u );
            ctrlDuty->forward_duty_r = dutyLimit( DUTY + ctrlDuty->forward_u );
            break;

        case Backward:
            ctrlDuty->backward_u = KP*eor + KI*(eor+ctrlDuty->backward_eor_1+ctrlDuty->backward_eor_2) + KD*(eor-ctrlDuty->backward_eor_1);
            ctrlDuty->backward_eor_2 = ctrlDuty->backward_eor_1;
            ctrlDuty->backward_eor_1 = eor;
            ctrlDuty->backward_duty_l = dutyLimit( DUTY - ctrlDuty->backward_u );
            ctrlDuty->backward_duty_r = dutyLimit( DUTY + ctrlDuty->backward_u );
            break;

        case TurnLeft:
            ctrlDuty->turnleft_duty_l = dutyLimit( DUTY_TURN_B );
            ctrlDuty->turnleft_duty_r = dutyLimit( DUTY_TURN_F );
            break;

        case TurnRight:
            ctrlDuty->turnright_duty_l = dutyLimit( DUTY_TURN_F );
            ctrlDuty->turnright_duty_r = dutyLimit( DUTY_TURN_B );
            break;
    }
}


