/*
 * ctrl.h
 *
 *  Created on: 2021��4��16��
 *      Author: Jpz
 */

#ifndef _CTRL_H_
#define _CTRL_H_

#include "car.h"

struct CTRL_carDuty{
    float forward_duty_l;
    float forward_duty_r;
    float forward_u;
    float forward_eor_1;
    float forward_eor_2;

    float backward_duty_l;
    float backward_duty_r;
    float backward_u;
    float backward_eor_1;
    float backward_eor_2;

    float turnleft_duty_l;
    float turnleft_duty_r;
    float turnleft_u;
    float turnleft_eor_1;
    float turnleft_eor_2;

    float turnright_duty_l;
    float turnright_duty_r;
    float turnright_u;
    float turnright_eor_1;
    float turnright_eor_2;
};

extern struct CTRL_carDuty ctrlDuty;
extern float KP;
extern float KI;
extern float KD;

void CTRL_Init(void);

void CTRL_CtrlDev(struct CTRL_carDuty *ctrlDuty, int eor, enum moveState mode);


#endif /* _CTRL_H_ */
