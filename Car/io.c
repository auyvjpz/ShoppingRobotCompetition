/*
 * io.c
 *
 *  Created on: 2021年4月15日
 *      Author: Jpz
 */

#include "DSP2833x_Device.h"
#include "DSP2833x_Examples.h"
#include "io.h"

#define IO_ID_HEAD 0
#define IO_ID_TAIL 1


//#define READIO funtion()

struct io_class io_head;
struct io_class io_tail;


void IO_Init(void)
{
    EALLOW;
    SysCtrlRegs.PCLKCR3.bit.GPIOINENCLK = 1;//打开IO口时钟

    //GPIO70
    GpioCtrlRegs.GPCMUX1.bit.GPIO70 = 0;//设置引脚为GPIO功能
    GpioCtrlRegs.GPCDIR.bit.GPIO70 = 0;//方向为输入
    GpioCtrlRegs.GPCPUD.bit.GPIO70 = 1;//上拉禁用
    //GPIO71
    GpioCtrlRegs.GPCMUX1.bit.GPIO71 = 0;//设置引脚为GPIO功能
    GpioCtrlRegs.GPCDIR.bit.GPIO71 = 0;//方向为输入
    GpioCtrlRegs.GPCPUD.bit.GPIO71 = 1;//上拉禁用
    //GPIO72
    GpioCtrlRegs.GPCMUX1.bit.GPIO72 = 0;//设置引脚为GPIO功能
    GpioCtrlRegs.GPCDIR.bit.GPIO72 = 0;//方向为输入
    GpioCtrlRegs.GPCPUD.bit.GPIO72 = 1;//上拉禁用
    //GPIO73
    GpioCtrlRegs.GPCMUX1.bit.GPIO73 = 0;//设置引脚为GPIO功能
    GpioCtrlRegs.GPCDIR.bit.GPIO73 = 0;//方向为输入
    GpioCtrlRegs.GPCPUD.bit.GPIO73 = 1;//上拉禁用
    //GPIO74
    GpioCtrlRegs.GPCMUX1.bit.GPIO74 = 0;//设置引脚为GPIO功能
    GpioCtrlRegs.GPCDIR.bit.GPIO74 = 0;//方向为输入
    GpioCtrlRegs.GPCPUD.bit.GPIO74 = 1;//上拉禁用
    //GPIO75
    GpioCtrlRegs.GPCMUX1.bit.GPIO75 = 0;//设置引脚为GPIO功能
    GpioCtrlRegs.GPCDIR.bit.GPIO75 = 0;//方向为输入
    GpioCtrlRegs.GPCPUD.bit.GPIO75 = 1;//上拉禁用
    //GPIO76
    GpioCtrlRegs.GPCMUX1.bit.GPIO76 = 0;//设置引脚为GPIO功能
    GpioCtrlRegs.GPCDIR.bit.GPIO76 = 0;//方向为输入
    GpioCtrlRegs.GPCPUD.bit.GPIO76 = 1;//上拉禁用
    //GPIO77
    GpioCtrlRegs.GPCMUX1.bit.GPIO77 = 0;//设置引脚为GPIO功能
    GpioCtrlRegs.GPCDIR.bit.GPIO77 = 0;//方向为输入
    GpioCtrlRegs.GPCPUD.bit.GPIO77 = 1;//上拉禁用
    //GPIO78
    GpioCtrlRegs.GPCMUX1.bit.GPIO78 = 0;//设置引脚为GPIO功能
    GpioCtrlRegs.GPCDIR.bit.GPIO78 = 0;//方向为输入
    GpioCtrlRegs.GPCPUD.bit.GPIO78 = 1;//上拉禁用
    //GPIO79
    GpioCtrlRegs.GPCMUX1.bit.GPIO79 = 0;//设置引脚为GPIO功能
    GpioCtrlRegs.GPCDIR.bit.GPIO79 = 0;//方向为输入
    GpioCtrlRegs.GPCPUD.bit.GPIO79 = 1;//上拉禁用

    EDIS;
    io_head.id = IO_ID_HEAD;
    io_head.io0 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO70;
    io_head.io1 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO71;
    io_head.io2 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO72;
    io_head.io3 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO73;
    io_head.io4 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO74;
    io_head.state = 0;
    io_head.value = 0;

    io_tail.id = IO_ID_TAIL;
    io_tail.io0 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO75;
    io_tail.io1 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO76;
    io_tail.io2 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO77;
    io_tail.io3 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO78;
    io_tail.io4 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO79;
    io_tail.state = 0;
    io_tail.value = 0;
}


int getState(char d0,char d1, char d2, char d3, char d4)
{
    unsigned int ret = 0;


    ret <<= 1;
    if(d0 != 0) ret++;

    ret <<= 1;
    if(d1 != 0) ret++;

    ret <<= 1;
    if(d2 != 0) ret++;

    ret <<= 1;
    if(d3 != 0) ret++;

    ret <<= 1;
    if(d4 != 0) ret++;

    return ret;
}

//int getValue(char d0,char d1, char d2, char d3, char d4)
int getValue(unsigned int state)
{
    static int ret = 0;
//    int k[9] = {0, 0, 0, 0, 1, 0, 0, 0, 0};

    state = state & 0x001f;
    if(state == 0x10)       ret=PB;//10000  0
    else if(state == 0x18)  ret=PM;//11000
    else if(state == 0x08)  ret=PS;//01000  1
    else if(state == 0x0c)  ret=PZ;//01100
    else if(state == 0x04)  ret=ZO;//00100  2
    else if(state == 0x06)  ret=NZ;//00110
    else if(state == 0x02)  ret=NS;//00010  3
    else if(state == 0x03)  ret=NM;//00011
    else if(state == 0x01)  ret=NB;//00001  4

    return ret;
}

int IO_getIoState(struct io_class *io_p)
{
    if(io_p->id == IO_ID_HEAD)
    {
        io_p->io0 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO70;
        io_p->io1 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO71;
        io_p->io2 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO72;
        io_p->io3 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO73;
        io_p->io4 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO74;
    }
    else if(io_p->id == IO_ID_TAIL)
    {
        io_p->io0 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO75;
        io_p->io1 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO76;
        io_p->io2 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO77;
        io_p->io3 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO78;
        io_p->io4 = (enum ioState)GpioDataRegs.GPCDAT.bit.GPIO79;
    }
    io_p->state = getState(io_p->io0, io_p->io1, io_p->io2, io_p->io3, io_p->io4);
    io_p->value = getValue(io_p->state);

    return io_p->value;
}


char IO_checkRisingEdge(struct io_class io)//检查上升沿，记录坐标用
{
    char ret = 0;
    static int state_last = 0x1f;//全亮
    if(io.state == 0x1f && state_last != 0x1f)
        ret = 1;//过线 上升沿
    else
        ret = 0;

    state_last = io.state;

    return ret;
}

