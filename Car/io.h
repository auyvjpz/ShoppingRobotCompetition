/*
 * io.h
 *
 *  Created on: 2021年4月15日
 *      Author: Jpz
 */

#ifndef _IO_H_
#define _IO_H_

#define PB  4
#define PM  3
#define PS  2
#define PZ  1
#define ZO  0
#define NZ  -1
#define NS  -2
#define NM  -3
#define NB  -4

enum ioState{//黑L  白H
    L,H
};

struct io_class
{//规定车身从 右向左 分别编号从 0 到 高

    enum ioState io0;
    enum ioState io1;
    enum ioState io2;
    enum ioState io3;
    enum ioState io4;
    char id;
    int value;
    int state;//记录亮灯个数
};
extern struct io_class io_head;
extern struct io_class io_tail;

void IO_Init(void);
int IO_getIoState(struct io_class *io_P);

char IO_checkRisingEdge(struct io_class io);



#endif /* _IO_H_ */
