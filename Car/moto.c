/*
 * moto.c
 *
 *  Created on: 2021年4月15日
 *      Author: Jpz
 */
#include "moto.h"
#include "DSP2833x_Device.h"
#include "DSP2833x_Examples.h"

#define TB_PRD 15000

struct MOTO_class moto_l;
struct MOTO_class moto_r;

void moto_basic_init(struct MOTO_class *moto)
{
    moto->duty = 0.0;//初始0占空比
    moto->dir = ToForward;
    if(moto->seat == seat_L)//初始化ePWM1
    {
        InitEPwm1Gpio(); //引脚初始化

        //时基单元配置
        EPwm1Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE; //不同步
        EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE; //不设置相位
        EPwm1Regs.TBPHS.half.TBPHS = 0;
        EPwm1Regs.TBCTR = 0; //清除计数
        EPwm1Regs.TBPRD = TB_PRD; //计数周期
        EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP; //向上计数
        EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
        EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;//TBCLK == SYSCLK == 150MHz
        //比较单元配置
        EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
        EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW; //影子模式
        EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
        EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;//零时刻装载
        EPwm1Regs.CMPA.half.CMPA = 0;
        EPwm1Regs.CMPB = 0; //设置初始比较值为零
        //动作单元配置
        EPwm1Regs.AQCTLA.bit.ZRO = AQ_SET; //计数为零时 高电平
        EPwm1Regs.AQCTLA.bit.CAU = AQ_CLEAR; //计数为向上A比较点时 低电平
        EPwm1Regs.AQCTLB.bit.ZRO = AQ_SET; //计数为零时 高电平
        EPwm1Regs.AQCTLB.bit.CBU = AQ_CLEAR; //计数为向上B比较点时 低电平
        //事件触发单元配置   中断配置
        EPwm1Regs.ETSEL.bit.INTSEL = ET_CTR_PRD;
        EPwm1Regs.ETSEL.bit.INTEN = 0; //不开启中断
        EPwm1Regs.ETPS.bit.INTPRD = ET_1ST;
    }
    if(moto->seat == seat_R)//初始化ePWM2
    {
        InitEPwm2Gpio(); //引脚初始化

        //时基单元配置
        EPwm2Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE; //不同步
        EPwm2Regs.TBCTL.bit.PHSEN = TB_DISABLE; //不设置相位
        EPwm2Regs.TBPHS.half.TBPHS = 0;
        EPwm2Regs.TBCTR = 0; //清除计数
        EPwm2Regs.TBPRD = TB_PRD; //计数周期
        EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP; //向上计数
        EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;
        EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;//TBCLK == SYSCLK == 150MHz
        //比较单元配置
        EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
        EPwm2Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW; //影子模式
        EPwm2Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
        EPwm2Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;//零时刻装载
        EPwm2Regs.CMPA.half.CMPA = 0;
        EPwm2Regs.CMPB = 0; //设置初始比较值为零
        //动作单元配置
        EPwm2Regs.AQCTLA.bit.ZRO = AQ_SET; //计数为零时 高电平
        EPwm2Regs.AQCTLA.bit.CAU = AQ_CLEAR; //计数为向上A比较点时 低电平
        EPwm2Regs.AQCTLB.bit.ZRO = AQ_SET; //计数为零时 高电平
        EPwm2Regs.AQCTLB.bit.CBU = AQ_CLEAR; //计数为向上B比较点时 低电平
        //事件触发单元配置   中断配置
        EPwm2Regs.ETSEL.bit.INTSEL = ET_CTR_PRD;
        EPwm2Regs.ETSEL.bit.INTEN = 0; //不开启中断
        EPwm2Regs.ETPS.bit.INTPRD = ET_1ST;
    }
}


void MOTO_Init(void)
{
    moto_l.seat = seat_L;
    moto_r.seat = seat_R;
    // 打开对应外设时钟
    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0; //先关闭时基模块时钟
    SysCtrlRegs.PCLKCR1.bit.EPWM1ENCLK = 1;
    SysCtrlRegs.PCLKCR1.bit.EPWM2ENCLK = 1;
    EDIS;

    moto_basic_init(&moto_l);
    moto_basic_init(&moto_r);

    //完成配置开启TB
    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1; //打开时基模块时钟
    EDIS;
}


unsigned int dutyToCmpx(float duty)
{
    return duty*TB_PRD/100;
}

void MOTO_updateMotoState(struct MOTO_class moto)
{
    if(moto.seat == seat_R)
    {
        if(moto.duty == 0)
        {
            EPwm1Regs.CMPA.half.CMPA = 0;
            EPwm1Regs.CMPB = 0;
        }
        else if(moto.dir == ToBackward)
        {
            EPwm1Regs.CMPA.half.CMPA = dutyToCmpx(moto.duty);
            EPwm1Regs.CMPB = 0;
        }
        else if(moto.dir == ToForward)
        {
            EPwm1Regs.CMPA.half.CMPA = 0;
            EPwm1Regs.CMPB = dutyToCmpx(moto.duty);
        }

    }

    else if(moto.seat == seat_L)
    {
        if(moto.duty == 0)
        {
            EPwm2Regs.CMPA.half.CMPA = 0;
            EPwm2Regs.CMPB = 0;
        }
        else if(moto.dir == ToBackward)
        {
            EPwm2Regs.CMPA.half.CMPA = dutyToCmpx(moto.duty);
            EPwm2Regs.CMPB = 0;
        }
        else if(moto.dir == ToForward)
        {
            EPwm2Regs.CMPA.half.CMPA = 0;
            EPwm2Regs.CMPB = dutyToCmpx(moto.duty);
        }
    }
}


