/*
 * moto.h
 *
 *  Created on: 2021年4月15日
 *      Author: Jpz
 */

#ifndef _MOTO_H_
#define _MOTO_H_

enum motoSeat{
    seat_L,seat_R
};
enum motoDir{
    ToForward,ToBackward
};

struct MOTO_class
{
    enum motoSeat seat;//区分电机安装位置
    enum motoDir dir;//电机带动车的方向
    float duty;//旋转占空比   0~98
};

extern struct MOTO_class moto_l;
extern struct MOTO_class moto_r;


void MOTO_Init(void);
void MOTO_updateMotoState(struct MOTO_class moto);

#endif /* _MOTO_H_ */
