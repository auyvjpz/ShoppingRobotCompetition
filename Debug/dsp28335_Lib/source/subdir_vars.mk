################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../dsp28335_Lib/source/DSP2833x_ADC_cal.asm \
../dsp28335_Lib/source/DSP2833x_CodeStartBranch.asm \
../dsp28335_Lib/source/DSP2833x_usDelay.asm 

C_SRCS += \
../dsp28335_Lib/source/DSP2833x_Adc.c \
../dsp28335_Lib/source/DSP2833x_CpuTimers.c \
../dsp28335_Lib/source/DSP2833x_DMA.c \
../dsp28335_Lib/source/DSP2833x_DefaultIsr.c \
../dsp28335_Lib/source/DSP2833x_ECap.c \
../dsp28335_Lib/source/DSP2833x_EPwm.c \
../dsp28335_Lib/source/DSP2833x_GlobalVariableDefs.c \
../dsp28335_Lib/source/DSP2833x_Gpio.c \
../dsp28335_Lib/source/DSP2833x_I2C.c \
../dsp28335_Lib/source/DSP2833x_MemCopy.c \
../dsp28335_Lib/source/DSP2833x_PieCtrl.c \
../dsp28335_Lib/source/DSP2833x_PieVect.c \
../dsp28335_Lib/source/DSP2833x_Sci.c \
../dsp28335_Lib/source/DSP2833x_Spi.c \
../dsp28335_Lib/source/DSP2833x_SysCtrl.c 

C_DEPS += \
./dsp28335_Lib/source/DSP2833x_Adc.d \
./dsp28335_Lib/source/DSP2833x_CpuTimers.d \
./dsp28335_Lib/source/DSP2833x_DMA.d \
./dsp28335_Lib/source/DSP2833x_DefaultIsr.d \
./dsp28335_Lib/source/DSP2833x_ECap.d \
./dsp28335_Lib/source/DSP2833x_EPwm.d \
./dsp28335_Lib/source/DSP2833x_GlobalVariableDefs.d \
./dsp28335_Lib/source/DSP2833x_Gpio.d \
./dsp28335_Lib/source/DSP2833x_I2C.d \
./dsp28335_Lib/source/DSP2833x_MemCopy.d \
./dsp28335_Lib/source/DSP2833x_PieCtrl.d \
./dsp28335_Lib/source/DSP2833x_PieVect.d \
./dsp28335_Lib/source/DSP2833x_Sci.d \
./dsp28335_Lib/source/DSP2833x_Spi.d \
./dsp28335_Lib/source/DSP2833x_SysCtrl.d 

OBJS += \
./dsp28335_Lib/source/DSP2833x_ADC_cal.obj \
./dsp28335_Lib/source/DSP2833x_Adc.obj \
./dsp28335_Lib/source/DSP2833x_CodeStartBranch.obj \
./dsp28335_Lib/source/DSP2833x_CpuTimers.obj \
./dsp28335_Lib/source/DSP2833x_DMA.obj \
./dsp28335_Lib/source/DSP2833x_DefaultIsr.obj \
./dsp28335_Lib/source/DSP2833x_ECap.obj \
./dsp28335_Lib/source/DSP2833x_EPwm.obj \
./dsp28335_Lib/source/DSP2833x_GlobalVariableDefs.obj \
./dsp28335_Lib/source/DSP2833x_Gpio.obj \
./dsp28335_Lib/source/DSP2833x_I2C.obj \
./dsp28335_Lib/source/DSP2833x_MemCopy.obj \
./dsp28335_Lib/source/DSP2833x_PieCtrl.obj \
./dsp28335_Lib/source/DSP2833x_PieVect.obj \
./dsp28335_Lib/source/DSP2833x_Sci.obj \
./dsp28335_Lib/source/DSP2833x_Spi.obj \
./dsp28335_Lib/source/DSP2833x_SysCtrl.obj \
./dsp28335_Lib/source/DSP2833x_usDelay.obj 

ASM_DEPS += \
./dsp28335_Lib/source/DSP2833x_ADC_cal.d \
./dsp28335_Lib/source/DSP2833x_CodeStartBranch.d \
./dsp28335_Lib/source/DSP2833x_usDelay.d 

OBJS__QUOTED += \
"dsp28335_Lib\source\DSP2833x_ADC_cal.obj" \
"dsp28335_Lib\source\DSP2833x_Adc.obj" \
"dsp28335_Lib\source\DSP2833x_CodeStartBranch.obj" \
"dsp28335_Lib\source\DSP2833x_CpuTimers.obj" \
"dsp28335_Lib\source\DSP2833x_DMA.obj" \
"dsp28335_Lib\source\DSP2833x_DefaultIsr.obj" \
"dsp28335_Lib\source\DSP2833x_ECap.obj" \
"dsp28335_Lib\source\DSP2833x_EPwm.obj" \
"dsp28335_Lib\source\DSP2833x_GlobalVariableDefs.obj" \
"dsp28335_Lib\source\DSP2833x_Gpio.obj" \
"dsp28335_Lib\source\DSP2833x_I2C.obj" \
"dsp28335_Lib\source\DSP2833x_MemCopy.obj" \
"dsp28335_Lib\source\DSP2833x_PieCtrl.obj" \
"dsp28335_Lib\source\DSP2833x_PieVect.obj" \
"dsp28335_Lib\source\DSP2833x_Sci.obj" \
"dsp28335_Lib\source\DSP2833x_Spi.obj" \
"dsp28335_Lib\source\DSP2833x_SysCtrl.obj" \
"dsp28335_Lib\source\DSP2833x_usDelay.obj" 

C_DEPS__QUOTED += \
"dsp28335_Lib\source\DSP2833x_Adc.d" \
"dsp28335_Lib\source\DSP2833x_CpuTimers.d" \
"dsp28335_Lib\source\DSP2833x_DMA.d" \
"dsp28335_Lib\source\DSP2833x_DefaultIsr.d" \
"dsp28335_Lib\source\DSP2833x_ECap.d" \
"dsp28335_Lib\source\DSP2833x_EPwm.d" \
"dsp28335_Lib\source\DSP2833x_GlobalVariableDefs.d" \
"dsp28335_Lib\source\DSP2833x_Gpio.d" \
"dsp28335_Lib\source\DSP2833x_I2C.d" \
"dsp28335_Lib\source\DSP2833x_MemCopy.d" \
"dsp28335_Lib\source\DSP2833x_PieCtrl.d" \
"dsp28335_Lib\source\DSP2833x_PieVect.d" \
"dsp28335_Lib\source\DSP2833x_Sci.d" \
"dsp28335_Lib\source\DSP2833x_Spi.d" \
"dsp28335_Lib\source\DSP2833x_SysCtrl.d" 

ASM_DEPS__QUOTED += \
"dsp28335_Lib\source\DSP2833x_ADC_cal.d" \
"dsp28335_Lib\source\DSP2833x_CodeStartBranch.d" \
"dsp28335_Lib\source\DSP2833x_usDelay.d" 

ASM_SRCS__QUOTED += \
"../dsp28335_Lib/source/DSP2833x_ADC_cal.asm" \
"../dsp28335_Lib/source/DSP2833x_CodeStartBranch.asm" \
"../dsp28335_Lib/source/DSP2833x_usDelay.asm" 

C_SRCS__QUOTED += \
"../dsp28335_Lib/source/DSP2833x_Adc.c" \
"../dsp28335_Lib/source/DSP2833x_CpuTimers.c" \
"../dsp28335_Lib/source/DSP2833x_DMA.c" \
"../dsp28335_Lib/source/DSP2833x_DefaultIsr.c" \
"../dsp28335_Lib/source/DSP2833x_ECap.c" \
"../dsp28335_Lib/source/DSP2833x_EPwm.c" \
"../dsp28335_Lib/source/DSP2833x_GlobalVariableDefs.c" \
"../dsp28335_Lib/source/DSP2833x_Gpio.c" \
"../dsp28335_Lib/source/DSP2833x_I2C.c" \
"../dsp28335_Lib/source/DSP2833x_MemCopy.c" \
"../dsp28335_Lib/source/DSP2833x_PieCtrl.c" \
"../dsp28335_Lib/source/DSP2833x_PieVect.c" \
"../dsp28335_Lib/source/DSP2833x_Sci.c" \
"../dsp28335_Lib/source/DSP2833x_Spi.c" \
"../dsp28335_Lib/source/DSP2833x_SysCtrl.c" 


