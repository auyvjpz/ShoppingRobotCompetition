/*
 * linkarm.c
 *
 *  Created on: 2021年5月25日
 *      Author: Administrator
 */

#include "linkarm.h"
#include "arm.h"
#include "DSP2833x_Device.h"
#include "DSP2833x_Examples.h"


struct LINKARM_Class linker;


char link_action(float degU, float degD)
{
    if(linker.State == LINK_STATE_STILL && linker.d7 == degU && linker.d8 == degD) return LINK_STATE_STILL;//已经到达

    linker.d7 = degU;
    linker.d8 = degD;
    linker.cmp7 = us2Cmp(degreeToD7Us(linker.d7));
    linker.cmp8 = us2Cmp(degreeToD8Us(linker.d8));
    linker.State = LINK_STATE_KINETIC;

    return LINK_STATE_STILL;
}

char link_updateDuty(void)
{
    if((D7CMP == linker.cmp7) && (D8CMP == linker.cmp8))//已经达到期望值
    {
        return LINK_STATE_STILL;//保持不动
    }

    D7CMP = setCmp(linker.cmp7, D7CMP,1);   //    EPwm6Regs.CMPA.half.CMPA
    D8CMP = setCmp(linker.cmp8, D8CMP,1);//    EPwm6Regs.CMPB
    return LINK_STATE_KINETIC;
}



void LINK_Init(void)
{
// 打开对应外设时钟
    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0; //先关闭时基模块时钟
    SysCtrlRegs.PCLKCR1.bit.EPWM6ENCLK = 1;
    EDIS;
//epwm6
    InitEPwm6Gpio(); //引脚初始化

    //时基单元配置
    EPwm6Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE; //不同步
    EPwm6Regs.TBCTL.bit.PHSEN = TB_DISABLE; //不设置相位
    EPwm6Regs.TBPHS.half.TBPHS = 0;
    EPwm6Regs.TBCTR = 0; //清除计数
    EPwm6Regs.TBPRD = LINK_TB_PRD; //计数周期
    EPwm6Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP; //向上计数
    EPwm6Regs.TBCTL.bit.HSPCLKDIV = 0x6;
    EPwm6Regs.TBCTL.bit.CLKDIV = 0x2;//50Hz
    //比较单元配置
    EPwm6Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm6Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW; //影子模式
    EPwm6Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;
    EPwm6Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;//零时刻装载
    EPwm6Regs.CMPA.half.CMPA = 0;
    EPwm6Regs.CMPB = 0; //设置初始比较值为零
    //动作单元配置
    EPwm6Regs.AQCTLA.bit.ZRO = AQ_SET; //计数为零时 高电平
    EPwm6Regs.AQCTLA.bit.CAU = AQ_CLEAR; //计数为向上A比较点时 低电平
    EPwm6Regs.AQCTLB.bit.ZRO = AQ_SET; //计数为零时 高电平
    EPwm6Regs.AQCTLB.bit.CBU = AQ_CLEAR; //计数为向上B比较点时 低电平
    //事件触发单元配置   中断配置
    EPwm6Regs.ETSEL.bit.INTSEL = ET_CTR_PRD;//计数为0时进入中断
    EPwm6Regs.ETSEL.bit.INTEN = 0; //开启中断
    EPwm6Regs.ETPS.bit.INTPRD = ET_1ST;
    EPwm6Regs.ETCLR.bit.INT = 1;//清除中断标志




    linker.d7 = 90.0;
    linker.d8 = 90.0;
    linker.cmp7 = us2Cmp(degreeToD7Us(linker.d7));
    linker.cmp8 = us2Cmp(degreeToD8Us(linker.d8));
    D7CMP = linker.cmp7;   //    EPwm6Regs.CMPA.half.CMPA
    D8CMP = linker.cmp8;//    EPwm6Regs.CMPB

//    linker.Mode = LINK_MODE_DEFINE;
    linker.State = LINK_STATE_KINETIC;

    linker.updateDuty = link_updateDuty;
    linker.action = link_action;

    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1; //打开时基模块时钟
    EDIS;
}



