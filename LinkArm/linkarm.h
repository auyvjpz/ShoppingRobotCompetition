/*
 * linkarm.h
 *
 *  Created on: 2021年5月25日
 *      Author: Jpz
 */

#ifndef _LINKARM_H_
#define _LINKARM_H_

#define LINK_TB_PRD 62500
#define D7CMP EPwm6Regs.CMPA.half.CMPA
#define D8CMP EPwm6Regs.CMPB

#define degreeToD7Us(x)   (1870.0 - x * 670.0 / 90.0)  //x范围：0   -  90    水平为0°
#define degreeToD8Us(x)   (880 + x * 650.0 / 90.0)     //x范围：0   -  180   朝向车尾为0°


#define LINK_DOWN_DEG 9.0
//#define LINK_MODE_DEFINE 0
//#define LINK_MODE_LINK   1

#define LINK_STATE_STILL    0
#define LINK_STATE_KINETIC  1



struct LINKARM_Class
{
    float d7;//04  3A       d  0   -  90  ===> 1870-  1200
    unsigned int cmp7;//04  3A
    float d8;//06  4A        d 0   -  90 - 180  ===>  880 -  1530  -  2180
    unsigned int cmp8;//06  4A

//    char Mode;
    char State;
    char (* updateDuty)(void);
    char (* action)(float,float);
};
extern struct LINKARM_Class linker;


void LINK_Init(void);

#endif /* _LINKARM_H_ */
