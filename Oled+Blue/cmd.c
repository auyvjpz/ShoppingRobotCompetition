/*
 * cmd.c
 *
 *  Created on: 2021年4月16日
 *      Author: Jpz
 */

#include "arm.h"
#include "cmd.h"
#include "cmdmap.h"
#include "uart.h"
#include "car.h"
#include "ctrl.h"
#include "flags.h"
#include "DSP2833x_Device.h"
#include "DSP2833x_Examples.h"
#include <string.h>
#include <math.h>


static char buf[BUFSIZE];//复制命令原始数据


/*
 *      各个指令行动的赋值函数
 *
 * */
static void CMD_CAMERA_LOOK_act(void)
{

}


#define STEP_DJ 20
static void CMD_ARM_SET_act(void)
{
    if(strstr(buf,"d1+")!=NULL) arm.d1 += STEP_DJ;
    if(strstr(buf,"d1-")!=NULL) arm.d1 -= STEP_DJ;
    if(strstr(buf,"d2+")!=NULL) arm.d2 += STEP_DJ;
    if(strstr(buf,"d2-")!=NULL) arm.d2 -= STEP_DJ;
    if(strstr(buf,"d3+")!=NULL) arm.d3 += STEP_DJ;
    if(strstr(buf,"d3-")!=NULL) arm.d3 -= STEP_DJ;
    if(strstr(buf,"d4+")!=NULL) arm.d4 += STEP_DJ;
    if(strstr(buf,"d4-")!=NULL) arm.d4 -= STEP_DJ;
    if(strstr(buf,"d5+")!=NULL) arm.d5 += STEP_DJ;
    if(strstr(buf,"d5-")!=NULL) arm.d5 -= STEP_DJ;
    if(strstr(buf,"d6+")!=NULL) arm.d6 += STEP_DJ;
    if(strstr(buf,"d6-")!=NULL) arm.d6 -= STEP_DJ;
    ARM_us2Cmps(&arm);
    D1CMP = arm.cmp1;
    D2CMP = arm.cmp2;
    D3CMP = arm.cmp3;
    D4CMP = arm.cmp4;
    D5CMP = arm.cmp5;
    D6CMP = arm.cmp6;

}
static void CMD_ARM_TK_act(void)
{
    if(strstr(buf,"STOP")!=NULL)        arm.action(ARM_ACTION_STOP);
    else if(strstr(buf,"EF")!=NULL)     arm.action(ARM_ACTION_DEFAULT);
    else if(strstr(buf,"WU")!=NULL)     arm.action(ARM_ACTION_WATCH_U);
    else if(strstr(buf,"WD")!=NULL)     arm.action(ARM_ACTION_WATCH_D);
    else if(strstr(buf,"UL")!=NULL)     arm.action(ARM_ACTION_GET_U_L);
    else if(strstr(buf,"UM")!=NULL)     arm.action(ARM_ACTION_GET_U_M);
    else if(strstr(buf,"UR")!=NULL)     arm.action(ARM_ACTION_GET_U_R);
    else if(strstr(buf,"DL")!=NULL)     arm.action(ARM_ACTION_GET_D_L);
    else if(strstr(buf,"DM")!=NULL)     arm.action(ARM_ACTION_GET_D_M);
    else if(strstr(buf,"DR")!=NULL)     arm.action(ARM_ACTION_GET_D_R);
}

static void CMD_CAR_STOP_act(void)
{
    car.cmd = Stop;
}

void CMD_CAR_FORWARD_act(void)
{
    if(car.axis_dir == faceToA)
        car.exp_y -= 1;
    else if(car.axis_dir == faceToB)
        car.exp_x += 1;
    else if(car.axis_dir == faceToC)
        car.exp_y += 1;
    if(car.axis_dir == faceToD)
        car.exp_x -= 1;
//    car.cmd = Forward;
}

void CMD_CAR_BACKWARD_act(void)
{
    if(car.axis_dir == faceToA)
        car.exp_y += 1;
    else if(car.axis_dir == faceToB)
        car.exp_x -= 1;
    else if(car.axis_dir == faceToC)
        car.exp_y -= 1;
    if(car.axis_dir == faceToD)
        car.exp_x += 1;
//    car.cmd = Backward;
}

void CMD_CAR_TURNLEFT_act(void)
{
    if(car.axis_dir == faceToA)
        car.exp_dir = faceToB;
    else if(car.axis_dir == faceToB)
        car.exp_dir = faceToC;
    else if(car.axis_dir == faceToC)
        car.exp_dir = faceToD;
    if(car.axis_dir == faceToD)
        car.exp_dir = faceToA;
}

void CMD_CAR_TURNRIGHT_act(void)
{
    if(car.axis_dir == faceToA)
        car.exp_dir = faceToD;
    else if(car.axis_dir == faceToB)
        car.exp_dir = faceToA;
    else if(car.axis_dir == faceToC)
        car.exp_dir = faceToB;
    if(car.axis_dir == faceToD)
        car.exp_dir = faceToC;
}

static void CMD_CTRL_PID_act(void)
{
    if(strstr(buf,"KP+")!=NULL) KP += 0.005;
    if(strstr(buf,"KP-")!=NULL) KP -= 0.005;
    if(strstr(buf,"KI+")!=NULL) KI += 0.001;
    if(strstr(buf,"KI-")!=NULL) KI -= 0.001;
    if(strstr(buf,"KD+")!=NULL) KD += 0.001;
    if(strstr(buf,"KD-")!=NULL) KD -= 0.001;

    SCI1_sendString("KP:");
    SCI1_sendString(float2str(KP,3));
    SCI1_sendString("   KI:");
    SCI1_sendString(float2str(KI,3));
    SCI1_sendString("   KD:");
    SCI1_sendString(float2str(KD,3));
    SCI1_sendString("\r\n");
}
static void CMD_CAR_XY_act(void)    //跑点 命令格式：  XY_x10y23_A\r\n
{
    char *p1;
    char *p2;
    int x = 0;
    int y = 0;

    //获取x
    p1 = strstr(buf,"x");
    p2 = strstr(buf,"y");
    p1++;
    while(strrchr("0123456789",*p1) != NULL)
    {
        x = x*10 + (*p1 - '0');
        p1++;
    }
    p2++;
    while(strrchr("0123456789",*p2) != NULL)
    {
        y = y*10 + (*p2 - '0');
        p2++;
    }

    car.exp_x = x;
    car.exp_y = y;
    if(strstr(buf,"_A")!=NULL) car.exp_dir = faceToA;
    else if(strstr(buf,"_B")!=NULL) car.exp_dir = faceToB;
    else if(strstr(buf,"_C")!=NULL) car.exp_dir = faceToC;
    else if(strstr(buf,"_D")!=NULL) car.exp_dir = faceToD;

    flag.car = 1;
}

static void CMD_DEBUG_act(void)
{
    flag.works = 1;
}


/***********************************
 *                            命令接收函数
 *
 * */

int CMD_Uart1_GetCmd(void)
{
    int ret = CMD_NULL;
//判断消息是否存有命令
    if(strstr(SCI1_RxBuf.buf,"mS")!=NULL)      ret=CMD_CAR_STOP;//停止
    else if(strstr(SCI1_RxBuf.buf,"mF")!=NULL) ret=CMD_CAR_FORWARD;//向前
    else if(strstr(SCI1_RxBuf.buf,"mB")!=NULL) ret=CMD_CAR_BACKWARD; //向后
    else if(strstr(SCI1_RxBuf.buf,"mL")!=NULL) ret=CMD_CAR_TURNLEFT;//向左转
    else if(strstr(SCI1_RxBuf.buf,"mR")!=NULL) ret=CMD_CAR_TURNRIGHT;//向右转
    else if(strstr(SCI1_RxBuf.buf,"PID")!=NULL)ret=CMD_CTRL_PID;//PID
    else if(strstr(SCI1_RxBuf.buf,"XY")!=NULL) ret=CMD_CAR_XY;//XY
    else if(strstr(SCI1_RxBuf.buf,"CM")!=NULL) ret=CMD_CAMERA_LOOK;//SHEXIANG
    else if(strstr(SCI1_RxBuf.buf,"AM")!=NULL) ret=CMD_ARM_SET;//ARM
    else if(strstr(SCI1_RxBuf.buf,"TK")!=NULL) ret=CMD_ARM_TK;//ARM_动作组
    else if(strstr(SCI1_RxBuf.buf,"GO")!=NULL) ret=CMD_DEBUG;//ARM_动作组


//如果有，将命令先记录下来
    if(ret != CMD_NULL)
    {
        memset(buf, '\0',BUFSIZE);
        strcpy(buf, SCI1_RxBuf.buf);
    }
    return ret;
}

int CMD_Uart1_ActionCmd(int cmd)
{
    if(cmd == CMD_NULL)                 return CMD_NULL;
    else if(cmd == CMD_CAR_STOP)        CMD_CAR_STOP_act();
    else if(cmd == CMD_CAR_FORWARD)     CMD_CAR_FORWARD_act();
    else if(cmd == CMD_CAR_BACKWARD)    CMD_CAR_BACKWARD_act();
    else if(cmd == CMD_CAR_TURNLEFT)    CMD_CAR_TURNLEFT_act();
    else if(cmd == CMD_CAR_TURNRIGHT)   CMD_CAR_TURNRIGHT_act();
    else if(cmd == CMD_CTRL_PID)        CMD_CTRL_PID_act();
    else if(cmd == CMD_CAR_XY)          CMD_CAR_XY_act();
    else if(cmd == CMD_CAMERA_LOOK)     CMD_CAMERA_LOOK_act();
    else if(cmd == CMD_ARM_SET)         CMD_ARM_SET_act();
    else if(cmd == CMD_ARM_TK)          CMD_ARM_TK_act();
    else if(cmd == CMD_DEBUG)            CMD_DEBUG_act();

    return cmd;
}





