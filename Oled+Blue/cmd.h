/*
 * cmd.h
 *
 *  Created on: 2021��4��16��
 *      Author: Administrator
 */

#ifndef _CMD_H_
#define _CMD_H_


void CMD_Init(void);

int CMD_Uart1_GetCmd(void);
int CMD_Uart1_ActionCmd(int cmd);


void CMD_CAR_FORWARD_act(void);
void CMD_CAR_BACKWARD_act(void);
void CMD_CAR_TURNLEFT_act(void);
void CMD_CAR_TURNRIGHT_act(void);


#endif /* _CMD_H_ */
