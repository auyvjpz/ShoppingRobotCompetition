/*
 * cmdmap.h
 *
 *  Created on: 2021年4月16日
 *      Author: Administrator
 */

#ifndef _CMDMAP_H_
#define _CMDMAP_H_

#define CMD_NULL 0

//关于小车移动
#define CMD_CAR_STOP       1
#define CMD_CAR_FORWARD    2
#define CMD_CAR_BACKWARD   3
#define CMD_CAR_TURNLEFT     4
#define CMD_CAR_TURNRIGHT    5
#define CMD_CAR_XY         6

#define CMD_CTRL_PID       7

#define CMD_CAMERA_LOOK    8

#define CMD_ARM_SET        9
#define CMD_ARM_TK         10

#define CMD_DEBUG           11

#endif /* _CMDMAP_H_ */
