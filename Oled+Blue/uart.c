/*
 * uart.c
 *
 *  Created on: 2021年4月15日
 *      Author: Jpz
 */
#include "uart.h"
#include "ctrl.h"
#include "cmd.h"
#include "DSP2833x_Device.h"
#include "DSP2833x_Examples.h"
#include <string.h>
#include <math.h>



#define END_FLAG '\n'

struct RxBuf SCI1_RxBuf;
struct RxBuf SCI2_RxBuf;


void SCI1_Basics_Init(Uint32 baud)
{
    unsigned char hBaud = 0;
    unsigned char lBaud = 0;
    Uint16 sciBaud = 0;

    sciBaud = 37500000 / (8 * baud) - 1;
    hBaud = sciBaud >> 8;
    lBaud = sciBaud & 0x00ff;

    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.SCIAENCLK = 1;
    EDIS;

    InitSciaGpio();

    SciaRegs.SCICCR.all = 0x0007;

    SciaRegs.SCICTL1.all = 0x0003;
    SciaRegs.SCICTL2.all = 0x0003;

    SciaRegs.SCIHBAUD = hBaud;
    SciaRegs.SCILBAUD = lBaud;
    SciaRegs.SCICTL1.all = 0x0023;
}

void SCI2_Basics_Init(Uint32 baud)
{
    unsigned char hBaud = 0;
    unsigned char lBaud = 0;
    Uint16 sciBaud = 0;

    sciBaud = 37500000 / (8 * baud) - 1;
    hBaud = sciBaud >> 8;
    lBaud = sciBaud & 0x00ff;

    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.SCIBENCLK = 1;
    EDIS;

    EALLOW;
    GpioCtrlRegs.GPAPUD.bit.GPIO18 = 0;    // Enable pull-up for GPIO18 (SCITXDB)
    GpioCtrlRegs.GPAPUD.bit.GPIO19 = 0;    // Enable pull-up for GPIO19 (SCIRXDB)
    GpioCtrlRegs.GPAQSEL2.bit.GPIO19 = 3;  // Asynch input GPIO19 (SCIRXDB)
    GpioCtrlRegs.GPAMUX2.bit.GPIO18 = 2;   // Configure GPIO18 for SCITXDB operation
    GpioCtrlRegs.GPAMUX2.bit.GPIO19 = 2;   // Configure GPIO19 for SCIRXDB operation
    EDIS;

    ScibRegs.SCICCR.all = 0x0007;

    ScibRegs.SCICTL1.all = 0x0003;
    ScibRegs.SCICTL2.all = 0x0003;

    ScibRegs.SCIHBAUD = hBaud;
    ScibRegs.SCILBAUD = lBaud;
    ScibRegs.SCICTL1.all = 0x0023;
}

void SCI1_Star_Receive(struct RxBuf *buf)
{
    buf->p = buf->buf;
    memset(buf->buf, '\0', BUFSIZE);
    SciaRegs.SCICTL1.bit.RXENA = 1;//打开接收使能
}
void SCI2_Star_Receive(void)
{
    SCI2_RxBuf.p = SCI2_RxBuf.buf;
    memset(SCI2_RxBuf.buf, '\0', BUFSIZE);
    ScibRegs.SCICTL1.bit.RXENA = 1;//打开接收使能
}

void SCI1_Stop_Receive(void)
{
    SciaRegs.SCICTL1.bit.RXENA = 0;//关闭接收使能
}
void SCI2_Stop_Receive(void)
{
    ScibRegs.SCICTL1.bit.RXENA = 0;//关闭接收使能
}

interrupt void SCI1_IRQ(void)
{
    if(SciaRegs.SCIRXST.bit.RXRDY == 1)//有数据可读
    {
        *(SCI1_RxBuf.p) = SciaRegs.SCIRXBUF.bit.RXDT;
        SCI1_RxBuf.p += 1;
        if(SCI1_RxBuf.p - SCI1_RxBuf.buf == BUFSIZE)//buf存满
        {
            SCI1_sendString("buf fulled \r\n");
            memset(SCI1_RxBuf.buf, '\0', BUFSIZE);
            SCI1_RxBuf.p = SCI1_RxBuf.buf;
        }
    }
    if(strstr(SCI1_RxBuf.buf,"\r\n") != NULL)//检测到完整指令
    {
//        SCI1_sendString(SCI1_RxBuf.buf);
        CMD_Uart1_ActionCmd( CMD_Uart1_GetCmd() );
        memset(SCI1_RxBuf.buf, '\0', BUFSIZE);
        SCI1_RxBuf.p = SCI1_RxBuf.buf;
    }
    PieCtrlRegs.PIEACK.bit.ACK9 = 1;//写1清零PIEACK对应位
}
interrupt void SCI2_IRQ(void)
{
    if(ScibRegs.SCIRXST.bit.RXRDY == 1)//有数据可读
    {
        *(SCI2_RxBuf.p) = ScibRegs.SCIRXBUF.bit.RXDT;
    }
    PieCtrlRegs.PIEACK.bit.ACK9 = 1;//写1清零PIEACK对应位
}


void SCI1_Init(unsigned long baud)
{
    SCI1_Basics_Init(baud);
    SCI1_Stop_Receive();

    DINT;//关闭CPU全局中断
    EALLOW;
    PieVectTable.SCIRXINTA = &SCI1_IRQ;//指定中断函数
    EDIS;
    PieCtrlRegs.PIEIER9.bit.INTx1 = 1;//使能对应PIE中断
    IER |= M_INT9;//使能cpu中断通道9
    EINT;//使能CPU全局中断
    ERTM;

    SCI1_Star_Receive(&SCI1_RxBuf);//开始接收
}
void SCI2_Init(unsigned long baud)
{
    SCI2_Basics_Init(baud);
    SCI2_Stop_Receive();

//    DINT;//关闭CPU全局中断
//    EALLOW;
//    PieVectTable.SCIRXINTB = &SCI2_IRQ;//指定中断函数
//    EDIS;
//    PieCtrlRegs.PIEIER9.bit.INTx3 = 1;//使能对应PIE中断
//    IER |= M_INT9;//使能cpu中断通道9
//    EINT;//使能CPU全局中断
//    ERTM;

    SCI2_Star_Receive();//开始接收
}

void SCI1_sendString(const char *str)
{
    int i,len;
    len = strlen(str);
    for(i=0;i<len;i++)
    {
        while(SciaRegs.SCICTL2.bit.TXEMPTY == 0);
        SciaRegs.SCITXBUF = str[i];
    }
}
void SCI2_sendDatas(const char *str,unsigned int len)
{
    int i;
    for(i=0;i<len;i++)
    {
        while(ScibRegs.SCICTL2.bit.TXEMPTY == 0);
        ScibRegs.SCITXBUF = str[i];
    }
}

int SCI2_reciveDatas(const char *str,int len,long waitTime)
{
    char i;
    long t;
    char *p;
    p = (char *)str;
//    memset((void *)str, '\0', len+2);
    for(i=0;i<len;i++)
    {
        t=0;
        while(ScibRegs.SCIRXST.bit.RXRDY != 1)
        {
            t++;
            if(t>waitTime)
                return p-str;
        }
        *p = ScibRegs.SCIRXBUF.bit.RXDT;
        p++;
    }
    return p-str;
}


/****************************************************/

char *float2str(float num,unsigned int w)
{
    static char str[17];
    char *p;
    int h,l;
    float tmp;
    char i;

    h = (int)num;
    tmp = (num - h)*pow(10,w);
    l = tmp;

    memset(str,'0',17);
    str[16] = '\0';
    p = &str[15];

    for(i=0;i<w;i++)
    {
        *p = l%10+'0';
        p--;
        l = l/10;
    }
    *p='.';
    p--;

    while(h!=0)
    {
        *p = h%10+'0';
        p--;
        h = h/10;
    }

    if(p[1]!='.') p++;

    return p;
}


char *uint2str(int num)
{
    static char str[17];
    char *p;
    int h;

    h = (int)num;

    memset(str,'0',17);
    str[16] = '\0';
    p = &str[15];

    if(h==0)
    {
        *p = '0';
        return p;
    }
    p++;
    while(h!=0)
    {
        p--;
        *p = h%10+'0';
        h = h/10;
    }
    return p;
}


