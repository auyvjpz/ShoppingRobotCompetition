/*
 * uart.h
 *
 *  Created on: 2021年4月15日
 *      Author: Jpz
 */

#ifndef _UART_H_
#define _UART_H_


#define BUFSIZE 48

struct RxBuf{
    char buf[BUFSIZE];
    char *p;
};

extern struct RxBuf SCI1_RxBuf;
extern struct RxBuf SCI2_RxBuf;


void SCI1_Init(unsigned long baud);
void SCI1_sendString(const char *str);

void SCI2_Init(unsigned long baud);
void SCI2_sendDatas(const char *str,unsigned int len);
int SCI2_reciveDatas(const char *str,int len,long waitTime);


char *float2str(float num,unsigned int w);
char *uint2str(int num);

interrupt void SCI1_IRQ(void);
interrupt void SCI2_IRQ(void);

#endif /* _UART_H_ */
