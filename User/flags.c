/*
 * flages.c
 *
 *  Created on: 2021年4月15日
 *      Author: Jpz
 */
#include "DSP2833x_Device.h"
#include "DSP2833x_Examples.h"
#include <flags.h>

struct flags_class flag;

void Flags_Init(void)
{
    GPIO_Init();
    flag.begin  = 1;
    flag.car    = 0;
    flag.pack   = 0;
    flag.works  = 0;
    flag.arm    = 0;
    flag.camera = 0;
    flag.speed = 0;
}

void GPIO_Init(void)
{
    EALLOW;
    SysCtrlRegs.PCLKCR3.bit.GPIOINENCLK = 1;
    EDIS;
    GpioCtrlRegs.GPBMUX2.bit.GPIO52 = 0;//GPIO模式
    GpioCtrlRegs.GPBMUX2.bit.GPIO53 = 0;
    GpioCtrlRegs.GPBMUX2.bit.GPIO54 = 0;
    GpioCtrlRegs.GPBMUX2.bit.GPIO56 = 0;
    GpioCtrlRegs.GPBMUX2.bit.GPIO57 = 0;

    GpioCtrlRegs.GPBDIR.bit.GPIO52 = 0;//输入模式
    GpioCtrlRegs.GPBDIR.bit.GPIO53 = 0;//输入模式
    GpioCtrlRegs.GPBDIR.bit.GPIO54 = 0;//输入模式
    GpioCtrlRegs.GPBDIR.bit.GPIO56 = 0;//输入模式
    GpioCtrlRegs.GPBDIR.bit.GPIO57 = 0;//输入模式

    GpioCtrlRegs.GPBPUD.bit.GPIO52 = 0;//默认上拉
    GpioCtrlRegs.GPBPUD.bit.GPIO53 = 0;//默认上拉
    GpioCtrlRegs.GPBPUD.bit.GPIO54 = 0;//默认上拉
    GpioCtrlRegs.GPBPUD.bit.GPIO56 = 0;//默认上拉
    GpioCtrlRegs.GPBPUD.bit.GPIO57 = 0;//默认上拉

}

