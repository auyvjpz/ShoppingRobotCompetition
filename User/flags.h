/*
 * flages.h
 *
 *  Created on: 2021��4��15��
 *      Author: Jpz
 */

#ifndef _FLAGS_H_
#define _FLAGS_H_


struct flags_class{

    unsigned char begin;
    unsigned char car;
    unsigned char camera;
    unsigned char arm;
    unsigned char works;
    unsigned char pack;
    unsigned char speed;
};
extern struct flags_class flag;

void Flags_Init(void);
void GPIO_Init(void);

#endif /* _FLAGS_H_ */
