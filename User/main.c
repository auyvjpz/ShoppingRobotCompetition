
/**
 * main.c
 */

#include "DSP2833x_Device.h"
#include "DSP2833x_Examples.h"
#include <string.h>
#include "uart.h"
#include "moto.h"
#include "car.h"
#include "io.h"
#include "ctrl.h"
#include "tim.h"
#include "flags.h"
#include "arm.h"
#include "camera.h"
#include "huskylens.h"
#include "cmd.h"
#include "linkarm.h"


char test;

void user_case1_DCBA(void);
void user_case1_E(void);

int main(void)
{

    MemCopy(&RamfuncsLoadStart, &RamfuncsLoadEnd, &RamfuncsRunStart);
    InitFlash();

    InitSysCtrl();//时钟初始化
    DINT;//禁止全局中断
    InitPieCtrl();//复位PIE
    IER = 0x0000;
    IFR = 0x0000;//清除CPU中断标志
    InitPieVectTable();//复位PIE中断向量
    EINT;//打开全局中断

///各项功能初始化////////////

    SCI1_Init(9600);
    SCI1_sendString("SCI1_Init OK!!\r\n");
    CMR_Init();
    SCI1_sendString("CMR_Init OK!!\r\n");
    Flags_Init();
    SCI1_sendString("Flags_Init OK!!\r\n");
    MOTO_Init();
    SCI1_sendString("MOTO_Init OK!!\r\n");
    CAR_Init();
    SCI1_sendString("CAR_Init OK!!\r\n");
    IO_Init();
    SCI1_sendString("IO_Init OK!!\r\n");
    CTRL_Init();
    SCI1_sendString("CTRL_Init OK!!\r\n");
    ARM_Init();
    SCI1_sendString("ARM_Init OK!!\r\n");
    LINK_Init();
    SCI1_sendString("LINK_Init OK!!\r\n");
    TIM2_Init();
    SCI1_sendString("TIM2_Init OK!!\r\n");
    CpuTimer2Regs.TCR.bit.TSS = 0;//开启定时器
///////////////////////////////////////////////////////////////////////
    arm.action(ARM_ACTION_DEFAULT);
    while(arm.actState != ARM_ACTION_STOP);

    if(GpioDataRegs.GPBDAT.bit.GPIO33 == 0) //    拨码开关2
    {
        while(1)
        {
            linker.action(LINK_DOWN_DEG, 0.0);
            flag.speed = 2;
            DELAY_US(333000);
            test = camera.classify(5);
            if(test == CMR_DOIT)
            {
                test = 0;
                arm.action(ARM_ACTION_GET_U_M);
                while(arm.actState != ARM_ACTION_STOP);
                arm.action(ARM_ACTION_WATCH_U_L);
                while(arm.actState != ARM_ACTION_STOP);
                DELAY_US(100000);
                test = camera.classify(3);
                if(test == CMR_DOIT)
                {
                    test = 0;
                    arm.action(ARM_ACTION_GET_U_L);
                    while(arm.actState != ARM_ACTION_STOP);
                }
                arm.action(ARM_ACTION_WATCH_U_R);
                while(arm.actState != ARM_ACTION_STOP);
                DELAY_US(100000);
                test = camera.classify(3);
                if(test == CMR_DOIT)
                {
                    test = 0;
                    arm.action(ARM_ACTION_GET_U_R);
                    while(arm.actState != ARM_ACTION_STOP);
                }
            }
        }
    }

    if(GpioDataRegs.GPBDAT.bit.GPIO55 == 0) // 测试购物车            拨码开关7
    {
        arm.action(ARM_ACTION_DEFAULT);
        while(arm.actState != ARM_ACTION_STOP);
        CMD_CAR_FORWARD_act();
        while(!(car.exp_x == car.axis_x && car.exp_y == car.axis_y && car.exp_dir == car.axis_dir));
        //车到位  阻断式放下连接爪
        linker.action(LINK_DOWN_DEG, 90.0);
        while(linker.State == LINK_STATE_KINETIC);
        user_case1_DCBA();
        user_case1_E();
    }
    if(GpioDataRegs.GPBDAT.bit.GPIO53 == 0) // 测试E     拨码开关6
    {
        arm.action(ARM_ACTION_DEFAULT);
        while(arm.actState != ARM_ACTION_STOP);
        CMD_CAR_FORWARD_act();
        while(!(car.exp_x == car.axis_x && car.exp_y == car.axis_y && car.exp_dir == car.axis_dir));
        //车到位  阻断式放下连接爪
        linker.action(LINK_DOWN_DEG, 90.0);
        while(linker.State == LINK_STATE_KINETIC);
        user_case1_E();
    }


    while(1)
    {
    }
}



////////////////////////////////////////////////////////////////////////////////////////
void user_case1_DCBA(void)
{
    int linkTmp;
    char i,I;
    unsigned int lst[5][3] = {  1,  4,  faceToC,
                                4,  10, faceToB,
                                10, 7,  faceToA,
                                7,  1,  faceToD,
                                1,  3,  faceToC};
    flag.speed = 1;
    for(I=0;I<5;I++)
    {
        arm.action(ARM_ACTION_DEFAULT);
        while(arm.actState != ARM_ACTION_STOP);
        linker.action(LINK_DOWN_DEG, 90.0);
        car.exp_x = lst[I][0];
        car.exp_y = lst[I][1];
        car.exp_dir = lst[I][2];
        linkTmp = car.axis_x + car.axis_y;
        while(linkTmp == (car.axis_x + car.axis_y));
        linker.action(LINK_DOWN_DEG, 0.0);
        while(!(car.exp_x == car.axis_x && car.exp_y == car.axis_y && car.exp_dir == car.axis_dir));
        if(I == 4) return;
//        while(linker.State == LINK_STATE_KINETIC);
        for(i=0;i<6;i++)
        {
            CMD_CAR_FORWARD_act();
            arm.action(ARM_ACTION_WATCH_U);
            while((arm.actState != ARM_ACTION_STOP) || (!(car.exp_x == car.axis_x && car.exp_y == car.axis_y && car.exp_dir == car.axis_dir)));
            if(camera.classify(5) == CMR_DOIT)
            {
                arm.action(ARM_ACTION_GET_U_M); while(arm.actState != ARM_ACTION_STOP);

                arm.action(ARM_ACTION_WATCH_U_L); while(arm.actState != ARM_ACTION_STOP);
                if(camera.classify(3) == CMR_DOIT)
                {
                    arm.action(ARM_ACTION_GET_U_L); while(arm.actState != ARM_ACTION_STOP);
                }
                arm.action(ARM_ACTION_WATCH_U_R); while(arm.actState != ARM_ACTION_STOP);
                if(camera.classify(3) == CMR_DOIT)
                {
                    arm.action(ARM_ACTION_GET_U_R); while(arm.actState != ARM_ACTION_STOP);
                }
            }

            arm.action(ARM_ACTION_WATCH_D);
            while(arm.actState != ARM_ACTION_STOP || car.self != Stop);
            if(camera.classify(1) == CMR_DOIT)
            {
                arm.action(ARM_ACTION_GET_D_M); while(arm.actState != ARM_ACTION_STOP);

                arm.action(ARM_ACTION_WATCH_D_L); while(arm.actState != ARM_ACTION_STOP);
                if(camera.classify(1) == CMR_DOIT)
                {
                    arm.action(ARM_ACTION_GET_D_L); while(arm.actState != ARM_ACTION_STOP);
                }
                arm.action(ARM_ACTION_WATCH_D_R); while(arm.actState != ARM_ACTION_STOP);
                if(camera.classify(1) == CMR_DOIT)
                {
                    arm.action(ARM_ACTION_GET_D_R); while(arm.actState != ARM_ACTION_STOP);
                }
            }
        }
    }/* 大循环结束 返回起始点 */

}

void user_case1_E(void)
{
    int linkTmp;
    char I;
    unsigned int lst[5][3] = {  4,  3,  faceToB,
                                8,  4,  faceToC,
                                7,  8,  faceToD,
                                3,  7,  faceToA,
                                4,  3,  faceToB };
    arm.action(ARM_ACTION_DEFAULT);
    while(arm.actState != ARM_ACTION_STOP);
    linker.action(LINK_DOWN_DEG, 90.0);
    car.exp_x = 3;
    car.exp_y = 3;
    car.exp_dir = faceToB;
    while(car.axis_dir != car.exp_dir);
    linker.action(LINK_DOWN_DEG, 0.0);
    while(!(car.exp_x == car.axis_x && car.exp_y == car.axis_y && car.exp_dir == car.axis_dir));
    /* 调整默认行进速度 调整循迹中点 */
    flag.speed = 2;
    linker.action(LINK_DOWN_DEG, 0.0);
    for(I=0;I<5;I++)
    {
        linker.action(LINK_DOWN_DEG, 0.0);
        car.exp_x = lst[I][0];
        car.exp_y = lst[I][1];
        car.exp_dir = lst[I][2];
//        linkTmp = car.axis_x + car.axis_y;
//        while(linkTmp == (car.axis_x + car.axis_y));
        while(!(car.exp_x == car.axis_x && car.exp_y == car.axis_y && car.exp_dir == car.axis_dir));
        if(I == 4)
        {
            linker.action(LINK_DOWN_DEG, 90.0);
            while(linker.State == LINK_STATE_KINETIC);
            flag.speed = 3;
            CMD_CAR_BACKWARD_act();CMD_CAR_BACKWARD_act();CMD_CAR_BACKWARD_act();
            return;
        }

        /* 伸出手臂准备观看 */
        arm.action(ARM_ACTION_WATCH_E);
        while(arm.actState != ARM_ACTION_STOP);
        CMD_CAR_FORWARD_act();CMD_CAR_FORWARD_act();CMD_CAR_FORWARD_act();//向前移动三格
        while(!(car.exp_x == car.axis_x && car.exp_y == car.axis_y && car.exp_dir == car.axis_dir))
        {
            if(camera.classify(1) == CMR_DOIT)//看见目标 进行抓取
            {
                /* 强制失能电机 */
                EPwm1Regs.AQCSFRC.bit.CSFA = 0x01;
                EPwm1Regs.AQCSFRC.bit.CSFB = 0x01;
                EPwm2Regs.AQCSFRC.bit.CSFA = 0x01;
                EPwm2Regs.AQCSFRC.bit.CSFB = 0x01;
                /* 机械臂动作 */
                arm.action(ARM_ACTION_GET_E);
                while(arm.actState != ARM_ACTION_STOP);
                arm.action(ARM_ACTION_WATCH_E);
                while(arm.actState != ARM_ACTION_STOP);
                /* 恢复使能电机 */
                EPwm1Regs.AQCSFRC.bit.CSFA = 0x00;
                EPwm1Regs.AQCSFRC.bit.CSFB = 0x00;
                EPwm2Regs.AQCSFRC.bit.CSFA = 0x00;
                EPwm2Regs.AQCSFRC.bit.CSFB = 0x00;
            }
        }

        /* 收回手臂 准备转弯 */
//        flag.speed = 3;
        arm.action(ARM_ACTION_DEFAULT);
        linker.action(LINK_DOWN_DEG, 90.0);
        CMD_CAR_FORWARD_act();
        while(arm.actState != ARM_ACTION_STOP);
        while(!(car.exp_x == car.axis_x && car.exp_y == car.axis_y && car.exp_dir == car.axis_dir));
    }

//    /* 调整默认行进速度 调整循迹中点 */
//    flag.speed = 3;
//    arm.action(ARM_ACTION_DEFAULT);
//    while(arm.actState != ARM_ACTION_STOP);
//    linker.action(LINK_DOWN_DEG, 90.0);
//    car.exp_x = 3;
//    car.exp_y = 2;
//    car.exp_dir = faceToA;
//    linkTmp = car.axis_x + car.axis_y;
//    while(linkTmp == (car.axis_x + car.axis_y));
//    while(!(car.exp_x == car.axis_x && car.exp_y == car.axis_y && car.exp_dir == car.axis_dir));

}

