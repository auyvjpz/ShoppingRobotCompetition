/*
 * tim.c
 *
 *  Created on: 2021年4月17日
 *      Author: Jpz
 */
#include <stdio.h>
#include <string.h>
#include "DSP2833x_Device.h"
#include "DSP2833x_Examples.h"
#include "uart.h"
#include "tim.h"
#include "io.h"
#include "ctrl.h"
#include "car.h"
#include "flags.h"
#include "arm.h"
#include "linkarm.h"

void TIM_Init(void)
{
    EALLOW;
//    SysCtrlRegs.PCLKCR3.bit.CPUTIMER0ENCLK = 1;
    SysCtrlRegs.PCLKCR3.bit.CPUTIMER1ENCLK = 1;
    SysCtrlRegs.PCLKCR3.bit.CPUTIMER2ENCLK = 1;
    EDIS;

    InitCpuTimers();
    ConfigCpuTimer(&CpuTimer1, 150, 1000);//用作计时
    ConfigCpuTimer(&CpuTimer2, 150, 1000);
}

void TIM1_Init(void)
{
//    unsigned int tmp=0;

    EALLOW;
    SysCtrlRegs.PCLKCR3.bit.CPUTIMER1ENCLK = 1;
//    PieVectTable.XINT13 = &TIM1_IQR;
    EDIS;

    // Initialize address pointers to respective timer registers:
    CpuTimer1.RegsAddr = &CpuTimer1Regs;
    // Initialize timer period to maximum:
    CpuTimer1Regs.PRD.all  = 0xFFFFFFFF;
    // Initialize pre-scale counter to divide by 1 (SYSCLKOUT):
    CpuTimer1Regs.TPR.all  = 0;
    CpuTimer1Regs.TPRH.all = 0;
    // Make sure timers are stopped:
    CpuTimer1Regs.TCR.bit.TSS = 1;
    // Reload all counter register with period value:
    CpuTimer1Regs.TCR.bit.TRB = 1;
    // Reset interrupt counters:
    CpuTimer1.InterruptCount = 0;

    ConfigCpuTimer(&CpuTimer1, 150, 1000);//完成基本定时

//    IER |= M_INT13;//打开定时器1中断
    IER &= ~M_INT13;//关闭定时器1中断
    CpuTimer1Regs.TCR.bit.TSS = 0;//开启定时器

    ERTM;
}

void TIM2_Init(void)
{
    EALLOW;
    SysCtrlRegs.PCLKCR3.bit.CPUTIMER2ENCLK = 1;
    PieVectTable.TINT2 = &TIM2_IQR;
    EDIS;

    // Initialize address pointers to respective timer registers:
    CpuTimer2.RegsAddr = &CpuTimer2Regs;
    // Initialize timer period to maximum:
    CpuTimer2Regs.PRD.all  = 0xFFFFFFFF;
    // Initialize pre-scale counter to divide by 1 (SYSCLKOUT):
    CpuTimer2Regs.TPR.all  = 0;
    CpuTimer2Regs.TPRH.all = 0;
    // Make sure timers are stopped:
    CpuTimer2Regs.TCR.bit.TSS = 1;
    // Reload all counter register with period value:
    CpuTimer2Regs.TCR.bit.TRB = 1;
    // Reset interrupt counters:
    CpuTimer2.InterruptCount = 0;

    ConfigCpuTimer(&CpuTimer2, 150, 1000);//完成基本定时

    IER |= M_INT14;//打开定时器2中断
    CpuTimer2Regs.TCR.bit.TSS = 1;//关闭定时器
//    CpuTimer2Regs.TCR.bit.TSS = 0;//开启定时器

    ERTM;
}

void loop_updateState(void)
{
    IO_getIoState(&io_head);//获取当前传感器状态
    IO_getIoState(&io_tail);
    flag.car = car.moveToXYD();//对车下达行动指令
    if(car.self == Forward)
        CTRL_CtrlDev(&ctrlDuty,io_head.value,Forward);//计算电机占空比
    else if(car.self == Backward)
        CTRL_CtrlDev(&ctrlDuty,io_tail.value,Backward);//计算电机占空比
    else
        CTRL_CtrlDev(&ctrlDuty,io_tail.value,car.self);//没什么大用

}
void loop_active(void)
{
    flag.arm = ARM_Active(&arm);
    CAR_MoveByCmd();//执行命令
}


interrupt void TIM2_IQR(void)//
{
    static Uint32 cut=0;//定时器进入次数  Uint32

    cut++;//每1ms递增


    if(cut%5 == 0)
    {
        loop_updateState();
//        loop_active();

        CAR_MoveByCmd();//执行命令
    }
    if(cut%7 == 0)
    {
        flag.arm = ARM_Active(&arm);

        linker.State = linker.updateDuty();
    }

    if(cut%500 == 0)
    {

        CAR_SendDataToCtrlDev();
        ARM_SendDataToCtrlDev();
    }

///////////////////////////////////////////////////////
    CpuTimer2Regs.TCR.bit.TIF = 1;//写1 清除中断标志位
}
