/*
 * tim.h
 *
 *  Created on: 2021��4��17��
 *      Author: Jpz
 */

#ifndef _TIM_H_
#define _TIM_H_


void TIM0_Init(void);
void TIM1_Init(void);
void TIM2_Init(void);
void TIM_Init(void);


void loop_active();
void loop_updateState();

interrupt void TIM2_IQR(void);

#endif /*_TIM_H_ */
